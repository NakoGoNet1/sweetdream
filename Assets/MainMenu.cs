﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
	public GameObject buttonForward;
	public GameObject buttonBackward;
	public GameObject buttonJump;
	public GameObject panel;

	public const string ForwardBtnName = "FORWARD_BTN";
	public const string BackwardBtnName = "BACKWARD_BTN";
	public const string JumpBtnName = "JUMP_BTN";
        
	private bool _wait = false;
	private string _waitBtnName; 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnGUI() {
		Event e = Event.current;
		if (e.isKey && _wait)
		{
			Debug.Log("Detected key code: " + e.keyCode);
			switch (_waitBtnName)
			{
					case ForwardBtnName:
						buttonForward.GetComponentInChildren<Text>().text = e.keyCode.ToString();
						break;
					case BackwardBtnName:
						buttonBackward.GetComponentInChildren<Text>().text = e.keyCode.ToString();
						break;
					case JumpBtnName:
						buttonJump.GetComponentInChildren<Text>().text = e.keyCode.ToString();
						break;
					default:
						break;
			}

			_wait = false;
			_waitBtnName = null;
			MenuSetState(true);
		}
	}

	public void OnForwardBtnClicked()
	{
		_wait = true;
		_waitBtnName = ForwardBtnName;
		MenuSetState(false);
	}

	public void OnBackwardBtnClicked()
	{
		_wait = true;
		_waitBtnName = BackwardBtnName;
		MenuSetState(false);
	}

	public void OnJumpBtnClicked()
	{
		_wait = true;
		_waitBtnName = JumpBtnName;
		MenuSetState(false);
	}

	public void StartGame()
	{
		panel.gameObject.SetActive(false);	
	}

	public void MenuSetState(bool state)
	{
		buttonForward.GetComponent<Button>().interactable = state; 
		buttonBackward.GetComponent<Button>().interactable = state; 
		buttonJump.GetComponent<Button>().interactable = state; 
	}
}
