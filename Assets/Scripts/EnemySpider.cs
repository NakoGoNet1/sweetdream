﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;//для работы со сценами

public class EnemySpider : MonoBehaviour
{
    public float moveSpeed = 1f;
    public GameObject MainAnim;
    public GameObject Player1; //подключаем игрока для понимания с какой он стороны для поворота врага
    public GameObject Player2;
    public GameObject PlayerCenter;
    public GameObject AggrePlayer;
    public GameObject LeftBorder;
    public GameObject RightBorder;
    public GameObject TopBorder;
    public GameObject BottomBorder;
    public GameObject LeftRange;
    public GameObject RightRange;
    public GameObject TopRange;
    public GameObject BottomRange;
    public GameObject Thread;

    public List<GameObject> StartPoints = new List<GameObject>();

    public int HP = 2;					// How many times the enemy can be hit before it dies.
    public float BirthTime;
    private bool isrun = false;
    private bool visible = false;
    public bool dead = true;          // Whether or not the enemy is dead.
    private bool live = false;
    private int Point = 1;
    public bool runOnPlayer = false;
    public bool FastStart = false; //паук не падает сверху, а бежит сразу при попадании игрока в "поле зрения"
    public bool OneLife = false;
    [Tooltip("in which side enemy will run after create (true = right false - left)")]
    public bool side = true; //с какой стороны игрок, в какую бежать врагу при создании (true = право false - лево)
    public bool BirthOutOfBorder = false;
    public float HightEnemyJump = 5550f;
    public GameObject MainImage;
    private bool OnScreen = true;
    private float dividerMove = 15f;
    //private bool turn = true; // в какую сторону бежать при соприкосновении с стеной (true = право false - лево)
    Animator anim;
    Rigidbody2D rb;
    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, new Vector3(LeftBorder.transform.position.x, LeftBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(RightBorder.transform.position.x, RightBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(TopBorder.transform.position.x, TopBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(BottomBorder.transform.position.x, BottomBorder.transform.position.y, 0.0f));

        Gizmos.DrawLine(new Vector3(LeftBorder.transform.position.x, TopBorder.transform.position.y, 0.0f), new Vector3(RightBorder.transform.position.x, TopBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(new Vector3(RightBorder.transform.position.x, TopBorder.transform.position.y, 0.0f), new Vector3(RightBorder.transform.position.x, BottomBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(new Vector3(RightBorder.transform.position.x, BottomBorder.transform.position.y, 0.0f), new Vector3(LeftBorder.transform.position.x, BottomBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(new Vector3(LeftBorder.transform.position.x, BottomBorder.transform.position.y, 0.0f), new Vector3(LeftBorder.transform.position.x, TopBorder.transform.position.y, 0.0f));

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, new Vector3(LeftRange.transform.position.x, LeftRange.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(RightRange.transform.position.x, RightRange.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(TopRange.transform.position.x, TopRange.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(BottomRange.transform.position.x, BottomRange.transform.position.y, 0.0f));

        Gizmos.DrawLine(new Vector3(LeftRange.transform.position.x, TopRange.transform.position.y, 0.0f), new Vector3(RightRange.transform.position.x, TopRange.transform.position.y, 0.0f));
        Gizmos.DrawLine(new Vector3(RightRange.transform.position.x, TopRange.transform.position.y, 0.0f), new Vector3(RightRange.transform.position.x, BottomRange.transform.position.y, 0.0f));
        Gizmos.DrawLine(new Vector3(RightRange.transform.position.x, BottomRange.transform.position.y, 0.0f), new Vector3(LeftRange.transform.position.x, BottomRange.transform.position.y, 0.0f));
        Gizmos.DrawLine(new Vector3(LeftRange.transform.position.x, BottomRange.transform.position.y, 0.0f), new Vector3(LeftRange.transform.position.x, TopRange.transform.position.y, 0.0f));

    }

    void Start()
    {

        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        rb.simulated = false;
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player2.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
    }

    IEnumerator waitToBirth()
    {

        yield return new WaitForSeconds(BirthTime);
        anim.SetTrigger("StartAnim");
        //rb.simulated = true;
        //MainAnim.GetComponent<Animator>().SetTrigger("2");//MAIN STARTTTTTTTTTTTTTTTT!!!!!!!!!!!!***************************************************
        rb.velocity = Vector2.zero;
        live = false;
        dead = false;
        //this.GetComponent<SpringJoint2D>().enabled = true;
        Thread.GetComponent<SpriteRenderer>().enabled = true;
        //StartCoroutine("waitToStop");

    }

    public void waitToStart()
    {
        //MainAnim.GetComponent<Animator>().SetTrigger("1");
        rb.simulated = true;
        rb.velocity = Vector2.zero;
        //this.GetComponent<SpringJoint2D>().enabled = false;
        Thread.GetComponent<SpriteRenderer>().enabled = false;
        //StartCoroutine("waitToStart");

    }

    //IEnumerator waitToStart()
    //{

    //    yield return new WaitForSeconds(0.5f);

    //    rb.simulated = true;
    //    rb.velocity = Vector2.zero;



    //}

    private void SelectAggrePlayer()
    {
        //select aggrePlayer
        if (Player1.GetComponent<Mario1Controller2DScript>().Dead == false && Player2.GetComponent<Mario1Controller2DScript>().Dead == false)
        {
            //Debug.Log(Random.Range(-2f, 2f));
            if (Random.Range(-2f, 2f) > 0)
            {
                AggrePlayer = Player2;
            }
            else
            {
                AggrePlayer = Player1;
            }
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            AggrePlayer = Player2;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            AggrePlayer = Player1;
        }
        if (Player1.GetComponent<Mario1Controller2DScript>().Dead == true && Player2.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            
        }
    }

    private void isOnScreen()
    {
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPosition.x < 0 || screenPosition.x > Screen.width || screenPosition.y < 0 || screenPosition.y > Screen.height)
        {
            OnScreen = false;
        }
        else
        {
            OnScreen = true;
        }
    }

    private void Visible()
    {
        //Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (PlayerCenter.transform.position.x > LeftBorder.transform.position.x && PlayerCenter.transform.position.x < RightBorder.transform.position.x &&
            PlayerCenter.transform.position.y > BottomBorder.transform.position.y && PlayerCenter.transform.position.y < TopBorder.transform.position.y)
        {
            visible = true;
        }
        else
        {
            visible = false;
        }
    }

    private void run()
    {
        isrun = true;//включаем переменную бега
        //rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем единичный вектор прыжка при начале бега
        if (FastStart == false)
        {
            anim.SetTrigger("Run");//включаем анимацию бега
        }
        else
        {
            anim.SetTrigger("RunFast");//включаем анимацию бега когда бежит без паузы при рождении
        }

    }

    private void checkIfOutOfScreen()
    {
        if (transform.position.x < LeftRange.transform.position.x || transform.position.x > RightRange.transform.position.x || 
            transform.position.y < BottomRange.transform.position.y || transform.position.y > TopRange.transform.position.y)
        {
            isOnScreen();
            if (OnScreen == false)
            {
                //Debug.Log("!!!!!!!!!!!!!!!!");
                //live = true;
                //dead = false;
                MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
                rb.simulated = false;
                isrun = false;
                anim.SetTrigger("End");
                CreatePoint();
                //transform.localPosition = new Vector2(0, 0);
                rb.velocity = Vector2.zero;
                dead = true;
            }
        }
    }

    private void CreatePoint()
    {
        //Debug.Log(Point);
        if (StartPoints[0] == null)
        {
            transform.localPosition = new Vector2(0, 0);
        }
        else
        {
            transform.localPosition = new Vector2(StartPoints[Point].transform.localPosition.x, StartPoints[Point].transform.localPosition.y);
            if (StartPoints[Point+1] == null)
            {
                Point = 0;
            }
            else
            {
                Point++;
            }

        }
    }


    private void create()
    {
        SelectAggrePlayer();
        Visible();
        if (visible == true)
        {
            if (live == false)
            {
                if (runOnPlayer == true)
                {
                    if (transform.position.x < AggrePlayer.transform.position.x)
                    {
                        side = true;
                    }
                    else
                    {
                        side = false;
                    }
                }


                if (FastStart == false)
                {
                    StartCoroutine("waitToBirth");
                    live = true;
                    rb.simulated = false;
                    isrun = false;
                    rb.velocity = Vector2.zero;
                    anim.SetTrigger("End");
                }
                else //fast start!!!!
                {
                    live = true;
                    rb.simulated = false;
                    isrun = false;
                    rb.velocity = Vector2.zero;
                    rb.simulated = true;
                    live = false;
                    dead = false;
                    run();
                    
                    Thread.GetComponent<SpriteRenderer>().enabled = false;
                }

            }
        }
    }
    private void isvisible()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)//обработка столкновений
    {
        if (collision.gameObject.tag == "Player")//если мы наткнулись на игрока
        {
            collision.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, false);//приносим ему урон
        }
        if (collision.gameObject.tag == "EnemyReturnColl")//если мы наткнулись на коллайдер розворота врага
        {
            rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
            if (side == true)//крутим врага в сторону когда он ударился в стенку
            {
                side = false;
            }
            else
            {
                side = true;
            }
        }
        if (collision.gameObject.tag == "EnemyJumpColl")//если мы наткнулись на коллайдер подпрыга врага
        {
            rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
        }

    }
    void FixedUpdate()
    {

        

        checkIfOutOfScreen();//проверка не вышел ли за екран наш враг

        if (dead == true && live == false)//если враг мертвый и ещё не создан запускаем функцию создания его
        {
            
            create();
        }
        if (GetComponent<EnemyHealth>().HP_enemy <= 0 && dead == false)//если у врага нету жизней убиваем его
        {
            if (OneLife == true)
            {
                Destroy(gameObject);
            }
            dead = true; live = false; GetComponent<EnemyHealth>().HP_enemy = HP;
            anim.SetTrigger("End");
            rb.simulated = false;
            isrun = false;
            CreatePoint();
            rb.velocity = Vector2.zero;
            anim.SetTrigger("Invisible");
            //MainAnim.GetComponent<Animator>().SetTrigger("1");//MAIN STARTTTTTTTTTTTTTTTT!!!!!!!!!!!!*************************************************************************

            dividerMove = 15f;
        }
        if (isrun == true && dead == false)//добавляем ему вектор бега каждый апдейт
        {

            if (side == true)
            {
                
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                rb.velocity = new Vector2(transform.localScale.x * moveSpeed / dividerMove, GetComponent<Rigidbody2D>().velocity.y);
                if (dividerMove <= 1f)
                {
                    dividerMove = 1f;
                }
                else
                {
                    dividerMove = dividerMove / 1.06f;
                }
                
            }
            else
            {
                
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                rb.velocity = new Vector2(transform.localScale.x * (-moveSpeed / dividerMove), GetComponent<Rigidbody2D>().velocity.y);
                if (dividerMove <= 1f)
                {
                    dividerMove = 1f;
                }
                else
                {
                    dividerMove = dividerMove / 1.06f;
                }
            }
        }

    }
}
