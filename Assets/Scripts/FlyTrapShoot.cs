﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyTrapShoot : MonoBehaviour
{

    public float HightEnemyJump = 5550f;
    public float DeathTime = 2f;
    private bool visible = true;

    void Start()
    {

        //Destroy(gameObject, 2f);
    }

    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.tag == "Player")
        {

            col.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, false);



            if (gameObject != null)
            {

                Destroy(gameObject);
            }
        }
    }

    private void Visible()
    {
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPosition.x < 0 || screenPosition.x > Screen.width || screenPosition.y < 0 || screenPosition.y > Screen.height)
        {
            visible = false;
            //Destroy(gameObject);
            //Debug.Log("Destroy");
        }
        else
        {
            visible = true;
        }
    }

    void Update()
    {
        Visible();
        if (visible == false)
        {
            StartCoroutine("waitToDeath");
        }
    }

    IEnumerator waitToDeath()
    {

        yield return new WaitForSeconds(DeathTime);
        Visible();
        if (visible == false)
        {
            if (gameObject != null)
            {

                Destroy(gameObject);
            }
        }
    }



}
