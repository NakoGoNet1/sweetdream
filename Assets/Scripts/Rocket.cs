﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour
{
    public GameObject explosion;        // Prefab of explosion effect.
    public GameObject explosionBig;
    public GameObject Boss;
    public int RocketPower;

    private AudioSource audioSource;
    public AudioClip[] shoot;
    private AudioClip shootClip;

    private bool shot = false;

    void Start()
    {
        // Destroy the rocket after 2 seconds if it doesn't get destroyed before then.
        //Destroy(gameObject, 0.5f);
        if (Boss.gameObject != null)
        {
            Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Boss.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
            //Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Boss.gameObject.GetComponent<CircleCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        }

        //Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<CapsuleCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    void Update ()
    {
        if (shot == false)
        {
            int index = Random.Range(0, shoot.Length);
            shootClip = shoot[index];
            audioSource.clip = shootClip;
            audioSource.Play();
            shot = true;
        }

        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPosition.x < 0 || screenPosition.x > Screen.width || screenPosition.y <0 || screenPosition.y > Screen.height)
        {
            if (this.gameObject.name == "rocket(Clone)")
            {
                Destroy(gameObject, 0.5f);
            }
            
        }
    }


    void OnExplode()
    {
        // Create a quaternion with a random rotation in the z-axis.
        Quaternion randomRotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));

        // Instantiate the explosion where the rocket is with the random rotation.
        Instantiate(explosion, new Vector2(transform.position.x, transform.position.y), randomRotation);
        
    }

    void OnExplodeBig()
    {
        // Create a quaternion with a random rotation in the z-axis.
        Quaternion randomRotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));

        // Instantiate the explosion where the rocket is with the random rotation.
        Instantiate(explosionBig, new Vector2(transform.position.x, transform.position.y), randomRotation);
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        // If it hits an enemy...
        if (col.tag == "Enemy")
        {
            // ... find the Enemy script and call the Hurt function.
            col.gameObject.GetComponent<EnemyHealth>().Hurt(RocketPower);
            if (col.GetComponent<EnemyHealth>().HP_enemy == 0)
            {
            Instantiate(explosionBig, new Vector2(col.gameObject.transform.position.x, col.gameObject.transform.position.y), Quaternion.identity);

                //explosionBig.transform.parent = col.transform;
                //OnExplodeBig(); Debug.Log("OnExplodeBig");
            }

            // Call the explosion instantiation.

            OnExplode();


            //Quaternion randomRotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));
            //GameObject nameOfGameObeject = Instantiate(explosion, new Vector2(transform.position.x, transform.position.y), randomRotation);
            //nameOfGameObeject.transform.parent = col.transform;

            // Destroy the rocket.
            Destroy(gameObject);
            //Destroy(gameObject, 0.5f);
        }
    }
}