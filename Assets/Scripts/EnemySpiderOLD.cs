﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;//для работы со сценами

public class EnemySpiderOLD : MonoBehaviour
{
    public float moveSpeed = 1f;
    public GameObject Player;
    public GameObject LeftBorder;
    public GameObject RightBorder;

    public List<GameObject> StartPoints = new List<GameObject>();

    public int HP = 2;					// How many times the enemy can be hit before it dies.
    public float BirthTime;
    private bool isrun = false;
    private bool visible = false;
    private bool dead = true;          // Whether or not the enemy is dead.
    private bool live = false;
    private int Point = 1;
    public bool runOnPlayer = false;
    public bool FastStart = false; //паук не падает сверху, а бежит сразу при попадании игрока в "поле зрения"
    public bool OneLife = false;
    [Tooltip("in which side enemy will run after create (true = right false - left)")]
    public bool side = true; //с какой стороны игрок, в какую бежать врагу при создании (true = право false - лево)
    public bool BirthOutOfBorder = false;
    public float HightEnemyJump = 5550f;
    public GameObject MainImage;
    //private bool turn = true; // в какую сторону бежать при соприкосновении с стеной (true = право false - лево)
    Animator anim;
    Rigidbody2D rb;
    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, new Vector3(LeftBorder.transform.position.x, LeftBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(RightBorder.transform.position.x, RightBorder.transform.position.y, 0.0f));

    }

    void Start()
    {

        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        rb.simulated = false;
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        //if (FastStart == true)
        //{
        //    create();
        //    run();
        //}
    }

    IEnumerator waitToBirth()
    {

        //live = true;
        //dead = false;
        //rb.simulated = false;
        //isrun = false;
        //transform.localPosition = new Vector2(0, 0);
        //GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        yield return new WaitForSeconds(BirthTime);
        anim.SetTrigger("StartAnim");
        rb.simulated = true;
        live = false;
        dead = false;
        //if (FastStart == true)
        //{
        //    yield return new WaitForSeconds(0f);
        //    //anim.SetTrigger("StartAnim");
        //    //run();
        //    rb.simulated = true;
        //    live = false;
        //    dead = false;
        //}
        //else
        //{
        //    yield return new WaitForSeconds(BirthTime);
        //    anim.SetTrigger("StartAnim");
        //    rb.simulated = true;
        //    live = false;
        //    dead = false;
        //}

    }

    private void Visible()
    {
        //Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (Player.transform.position.x > LeftBorder.transform.position.x && Player.transform.position.x < RightBorder.transform.position.x)
        {
            visible = true;
        }
        else
        {
            visible = false;
        }
    }

    private void run()
    {
        isrun = true;//включаем переменную бега
        GetComponent<Rigidbody2D>().AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем единичный вектор прыжка при начале бега
        if (FastStart == false)
        {
            anim.SetTrigger("Run");//включаем анимацию бега
        }
        else
        {
            anim.SetTrigger("RunFast");//включаем анимацию бега когда бежит без паузы при рождении
        }
        
    }

    private void checkIfOutOfScreen()
    {
        if (transform.position.x < LeftBorder.transform.position.x && BirthOutOfBorder == false || transform.position.x > RightBorder.transform.position.x && BirthOutOfBorder == false)
        {//BirthOutOfBorder == false если враг может создаваться только в пределах range
            Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
            if (screenPosition.x < 0 || screenPosition.x > Screen.width || screenPosition.y < 0 || screenPosition.y > Screen.height)
            { 
                //Debug.Log("!!!!!!!!!!!!!!!!");
            //live = true;
            //dead = false;
            MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f); 
            rb.simulated = false;
            isrun = false;
            anim.SetTrigger("End");
            CreatePoint();
            //transform.localPosition = new Vector2(0, 0);
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            dead = true;
            //anim.SetTrigger("StartAnim");
                //rb.simulated = true;
            }
        }
        if (BirthOutOfBorder == true)//BirthOutOfBorder == true если враг может создаваться за пределами range
        {
            Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
            if (screenPosition.x < -(Screen.width / 2.5) || screenPosition.x > Screen.width + (Screen.width / 2.5) || screenPosition.y < 0 || screenPosition.y > Screen.height)
            {
                //Debug.Log("screenPosition.x = " + screenPosition.x + " screenPosition.y = " + screenPosition.y);
                //Debug.Log("!!!!!!!!!!!!!!!!");
                //live = true;
                //dead = false;
                MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
                rb.simulated = false;
                isrun = false;
                anim.SetTrigger("End");
                CreatePoint();
                //transform.localPosition = new Vector2(0, 0);
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                dead = true;
                //anim.SetTrigger("StartAnim");
                //rb.simulated = true;
            }
        }
    }

    private void CreatePoint()
    {
        //Debug.Log(Point);
        if (StartPoints[0] == null)
        {
            transform.localPosition = new Vector2(0, 0);
        }
        else
        {
            transform.localPosition = new Vector2(StartPoints[Point].transform.localPosition.x, StartPoints[Point].transform.localPosition.y);
            if (StartPoints[Point+1] == null)
            {
                Point = 0;
            }
            else
            {
                Point++;
            }
            //if (StartPoints[Point] != null)
            //{
            //    transform.localPosition = new Vector2(StartPoints[Point].transform.localPosition.x, StartPoints[Point].transform.localPosition.y);
            //    Point++;
            //}
            //else
            //{
            //    Point = 0;
            //    transform.localPosition = new Vector2(StartPoints[Point].transform.localPosition.x, StartPoints[Point].transform.localPosition.y);
            //}

        }
        //transform.localPosition = new Vector2(StartPoints[0].transform.localPosition.x, StartPoints[0].transform.localPosition.y);
        //Debug.Log("X = " + StartPoints[0].transform.localPosition.x);
        //Debug.Log("Y = " + StartPoints[0].transform.localPosition.y);
    }


    private void create()
    {

        Visible();
        if (visible == true)
        {
            if (live == false)
            {
                if (runOnPlayer == true)
                {
                    if (transform.position.x < Player.transform.position.x)
                    {
                        side = true;
                    }
                    else
                    {
                        side = false;
                    }
                }

                //Debug.Log(StartPoints.Count);
                //Debug.Log("!!!!!!!!!!!!!!!!");


                if (FastStart == false)
                {
                    StartCoroutine("waitToBirth");
                    live = true;
                    //dead = false;
                    rb.simulated = false;
                    isrun = false;
                    //CreatePoint();
                    //transform.localPosition = new Vector2(0, 0);
                    GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                    //anim.SetTrigger("StartAnim");
                    anim.SetTrigger("End");
                    //rb.simulated = true;
                }
                else
                {
                    live = true;
                    rb.simulated = false;
                    isrun = false;
                    //CreatePoint();
                    //transform.localPosition = new Vector2(0, 0);
                    GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                    //anim.SetTrigger("End");
                    //anim.SetTrigger("StartAnim");
                    rb.simulated = true;
                    live = false;
                    dead = false;
                    run();
                }

            }
        }
    }
    private void isvisible()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)//обработка столкновений
    {
        if (collision.gameObject.tag == "Player")//если мы наткнулись на игрока
        {
            collision.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, false);//приносим ему урон
        }
        if (collision.gameObject.tag == "EnemyReturnColl")//если мы наткнулись на коллайдер розворота врага
        {
            GetComponent<Rigidbody2D>().AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
            if (side == true)//крутим врага в сторону когда он ударился в стенку
            {
                side = false;
            }
            else
            {
                side = true;
            }
        }
        if (collision.gameObject.tag == "EnemyJumpColl")//если мы наткнулись на коллайдер подпрыга врага
        {
            GetComponent<Rigidbody2D>().AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
        }

    }
    void FixedUpdate()
    {

        

        checkIfOutOfScreen();//проверка не вышел ли за екран наш враг

        if (dead == true && live == false)//если враг мертвый и ещё не создан запускаем функцию создания его
        {
            
            create();
        }
        if (GetComponent<EnemyHealth>().HP_enemy <= 0 && dead == false)//если у врага нету жизней убиваем его
        {
            if (OneLife == true)
            {
                Destroy(gameObject);
            }
            dead = true; live = false; GetComponent<EnemyHealth>().HP_enemy = HP;
            anim.SetTrigger("End");
            rb.simulated = false;
            isrun = false;
            CreatePoint();
            //transform.localPosition = new Vector2(0, 0);
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
        if (isrun == true && dead == false)//добавляем ему вектор бега каждый апдейт
        {

            if (side == true)
            {
                
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x * moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x * (-moveSpeed), GetComponent<Rigidbody2D>().velocity.y);
            }
        }

    }
}
