﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCorgy : MonoBehaviour {


    public float moveSpeed = 1f;
    public GameObject Player1;
    public GameObject Player2;
    public GameObject PlayerCenter;
    public GameObject AggrePlayer;
    public GameObject LeftBorder;
    public GameObject RightBorder;
    public GameObject LeftRange;
    public GameObject RightRange;
    public GameObject PointBorder1;
    public GameObject PointBorder2;
    public int HP = 2;
    public float BirthTime;
    public float HightEnemyJump = 5550f;
    public GameObject MainImage;
    private bool visible = false;
    private bool visiblePoint = false;
    private bool dead = true;          // Whether or not the enemy is dead.
    public bool side = true;
    private bool live = false;
    private bool run = false;
    private bool pause = false;
    Animator anim;
    Rigidbody2D rb;
    Vector2 velocity;

    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, new Vector3(LeftBorder.transform.position.x, LeftBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(RightBorder.transform.position.x, RightBorder.transform.position.y, 0.0f));
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, new Vector3(LeftRange.transform.position.x, LeftRange.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(RightRange.transform.position.x, RightRange.transform.position.y, 0.0f));

    }

    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player2.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        transform.localPosition = new Vector2(0, 0);
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        rb.simulated = false;
    }

    private void Visible()
    {
        //Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (PlayerCenter.transform.position.x > LeftBorder.transform.position.x && PlayerCenter.transform.position.x < RightBorder.transform.position.x)
        {
            visible = true;
        }
        else
        {
            visible = false;
        }
    }

    private void VisiblePoint()//видим ли корги на екране (что бы не создавать когда видно точку создания)
    {
        Vector2 screenPosition1 = Camera.main.WorldToScreenPoint(PointBorder1.transform.position);
        Vector2 screenPosition2 = Camera.main.WorldToScreenPoint(PointBorder2.transform.position);
        if (screenPosition1.x > 0 && screenPosition1.x < Screen.width && screenPosition1.y > 0 && screenPosition1.y < Screen.height || screenPosition2.x > 0 && screenPosition2.x < Screen.width && screenPosition2.y > 0 && screenPosition2.y < Screen.height)
        {
            visiblePoint = true;
        }
        else
        {
            visiblePoint = false;
        }
    }

    private void checkIfOutOfScreen()
    {
        if (transform.position.x < LeftRange.transform.position.x || transform.position.x > RightRange.transform.position.x)
        {   
            transform.localPosition = new Vector2(0, 0);
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            rb.simulated = false;
            dead = true;
        }
    }

    private void create()
    {
        Visible();
        if (visible == true)
        {

            if (transform.position.x < AggrePlayer.transform.position.x)
            {
                side = true;
            }
            else
            {
                side = false;
            }

            StartCoroutine("waitToBirth");
            live = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)//обработка столкновений
    {
        if (collision.gameObject.tag == "Player")//если мы наткнулись на игрока
        {
            collision.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, false);//приносим ему урон
        }
        if (collision.gameObject.tag == "EnemyReturnColl")//если мы наткнулись на коллайдер розворота врага
        {
            GetComponent<Rigidbody2D>().AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
            if (side == true)//крутим врага в сторону когда он ударился в стенку
            {
                side = false;
            }
            else
            {
                side = true;
            }
        }
        if (collision.gameObject.tag == "EnemyJumpColl")//если мы наткнулись на коллайдер подпрыга врага
        {
            GetComponent<Rigidbody2D>().AddForce(transform.up * 2f * 2, ForceMode2D.Impulse);//добавляем вектор подпрыга
            anim.SetTrigger("Jump");
        }

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Floor" || other.gameObject.tag == "MovedPlatform")//
        {
            anim.SetTrigger("Run");
            run = true;
        }

    }

    void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.tag == "Floor" || other.gameObject.tag == "MovedPlatform")//
        {
            //anim.SetTrigger("Run");
            run = true;
        }

    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Floor" || other.gameObject.tag == "MovedPlatform")//
        {
            //anim.SetTrigger("Run");
            run = false;
        }

    }


    void FixedUpdate()
    {
        //Debug.Log(run);


        checkIfOutOfScreen();//проверка не вышел ли за екран наш враг


        if (dead == true && live == false)//если враг мертвый и ещё не создан запускаем функцию создания его
        {
            VisiblePoint();
            if (visiblePoint == false)
            {
            create();
            }
        }
        if (GetComponent<EnemyHealth>().HP_enemy <= 0)//если у врага нету жизней убиваем его && dead == false
        {
            dead = true; live = false; GetComponent<EnemyHealth>().HP_enemy = HP;
            rb.simulated = false;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            transform.localPosition = new Vector2(0, 0);
            anim.SetTrigger("Default");

        }
        if (dead == false && run == true && pause == false)//добавляем ему вектор бега каждый апдейт
        {

            if (side == true)
            {

                transform.localRotation = Quaternion.Euler(0, 180, 0);
                GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x * moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {

                transform.localRotation = Quaternion.Euler(0, 0, 0);
                GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x * (-moveSpeed), GetComponent<Rigidbody2D>().velocity.y);
            }
        }

    }
    // Use this for initialization

    IEnumerator waitToBirth()
    {
        yield return new WaitForSeconds(BirthTime);
        anim.SetTrigger("Default");
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        rb.simulated = true;
        live = false;
        dead = false;

    }

    private void StartPause()
    {
        velocity = rb.velocity;
        rb.isKinematic = true;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = 0;
        anim.enabled = false;
    }

    private void StopPause()
    {
        rb.isKinematic = false;
        rb.velocity = velocity;
        anim.enabled = true;

    }

    // Update is called once per frame
    void Update () {

        if (Input.GetKeyUp(KeyCode.P))
        {
            if(pause == false)
            {
                pause = true;
                StartPause();
            }
            else
            {
                pause = false;
                StopPause();
            }
        }

    }
}
