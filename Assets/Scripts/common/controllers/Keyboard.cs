﻿using Boo.Lang;
using common.player;
using UnityEngine;

namespace common.controllers
{
    public class Keyboard : MonoBehaviour
    {
        private List<Player> _playerSubscribers;

        public Keyboard()
        {
            _playerSubscribers = new List<Player>();
        }

        public void Subscribe(Player player)
        {
            _playerSubscribers.Add(player);
        }
        
        

        private void Update()
        {
            
        }
    }
}