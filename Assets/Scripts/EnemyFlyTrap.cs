﻿using UnityEngine;
using System.Collections;

public class EnemyFlyTrap : MonoBehaviour
{
    //public float moveSpeed = 2f;		// The speed the enemy moves at.
    public int HP = 2;					// How many times the enemy can be hit before it dies.
    public float TimeRespaun = 2f;
    //private int HP_enemy; //сколько жизней в врага в данный момент
    private bool side = false; //в какую сторону смотрит враг лево - false право - true
    public GameObject Player1; //подключаем игрока для понимания с какой он стороны для поворота врага
    public GameObject Player2; //подключаем игрока для понимания с какой он стороны для поворота врага
    public GameObject PlayerCenter; //подключаем игрока для понимания с какой он стороны для поворота врага
    public GameObject AggrePlayer;
    public Rigidbody2D rocket; //подключаем пулю врага
    public float BulletSpeed = 20f; //скорость пули врага
    //public Sprite deadEnemy;			// A sprite of the enemy when it's dead.
    //public Sprite damagedEnemy;			// An optional sprite of the enemy when it's damaged.
    //public AudioClip[] deathClips;		// An array of audioclips that can play when the enemy dies.
    //public GameObject hundredPointsUI;	// A prefab of 100 that appears when the enemy dies.
    //public float deathSpinMin = -100f;			// A value to give the minimum amount of Torque when dying
    //public float deathSpinMax = 100f;			// A value to give the maximum amount of Torque when dying
    Animator anim;

    //private SpriteRenderer ren;			// Reference to the sprite renderer.
    //private Transform frontCheck;		// Reference to the position of the gameobject used for checking if something is in front.
    private bool dead = true;          // Whether or not the enemy is dead.
    private bool live = false;
    private bool visible = false;
    private bool shoot = false;
    public float HightEnemyJump = 5550f;
    public GameObject LeftBorder;
    public GameObject RightBorder;
    public GameObject MainImage;

    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, new Vector3(LeftBorder.transform.position.x, LeftBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(RightBorder.transform.position.x, RightBorder.transform.position.y, 0.0f));

    }
    //void Awake()
    //{
    //	// Setting up the references.
    //	ren = transform.Find("body").GetComponent<SpriteRenderer>();
    //	frontCheck = transform.Find("frontCheck").transform;
    //	score = GameObject.Find("Score").GetComponent<Score>();
    //}
    void Start()
    {
        anim = GetComponent<Animator>();
        gameObject.tag = "Untagged";


    }
    private void OnTriggerStay2D(Collider2D collision)//обработка столкновений
    {
        if (collision.gameObject.tag == "Player")//если мы наткнулись на игрока
        {
            collision.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, false);//приносим ему урон
        }
    }

    IEnumerator waitToBirth()
    {
        yield return new WaitForSeconds(TimeRespaun); anim.SetTrigger("Birth"); dead = false; GetComponent<EnemyHealth>().HP_enemy = HP; //Debug.Log("StartAnim1");
        gameObject.tag = "Enemy";
        SelectAggrePlayer();
        //Debug.Log("!!!!!!!!!!!!!!");
    }
    IEnumerator waitToShoot()
    {
        yield return new WaitForSeconds(TimeRespaun); anim.SetTrigger("Shoot"); shoot = false;

        //Debug.Log("!!!!!!!!!!!!!!");
    }

    private void SelectAggrePlayer()
    {
        //select aggrePlayer
        if (Player1.GetComponent<Mario1Controller2DScript>().Dead == false && Player2.GetComponent<Mario1Controller2DScript>().Dead == false)
        {
            //Debug.Log(Random.Range(-2f, 2f));
            if (Random.Range(-2f, 2f) > 0)
            {
                AggrePlayer = Player2;
            }
            else
            {
                AggrePlayer = Player1;
            }
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            AggrePlayer = Player2;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            AggrePlayer = Player1;
        }
    }


    private void Visible()
    {
        //Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (PlayerCenter.transform.position.x > LeftBorder.transform.position.x && PlayerCenter.transform.position.x < RightBorder.transform.position.x)
        {
            visible = true;
        }
        else
        {
            visible = false;
        }
    }

    void FixedUpdate()
    {

        if (dead == true && live == false) //если враг мертвый и ещё не созданный создаем его
        {
            Visible();
            if (visible == true) //если враг находится в поле зрения камеры
            { StartCoroutine("waitToBirth"); live = true; }
        }
        // If the enemy has zero or fewer hit points and isn't dead yet...
        if (GetComponent<EnemyHealth>().HP_enemy <= 0 && !dead)
            // ... call the death function.
            Death();
        if (transform.position.x > AggrePlayer.transform.position.x) // поворот врага в зависимости с какой стороны игрок
        {
            MainImage.transform.localRotation = Quaternion.Euler(0, 0, 0); side = false;
        }
        else
        {
            MainImage.transform.localRotation = Quaternion.Euler(0, 180, 0); side = true;
        }
        //выстрел врага
        if (dead == false && live == true && shoot == false)
        {
            Visible();
            if (visible == true) //если враг находится в поле зрения камеры
            {
                StartCoroutine("waitToShoot"); shoot = true;
            }
        }
    }

    private void Shoot()
    {

        if (side == false)
        {
            Rigidbody2D bulletInstance = Instantiate(rocket, new Vector2(transform.position.x - 1.5f, transform.position.y - 0.2f), Quaternion.Euler(new Vector3(0, -180f, 0))) as Rigidbody2D;
            bulletInstance.velocity = new Vector2(-BulletSpeed, 0); //Debug.Log("Shoot");
        }
        else
        {
            Rigidbody2D bulletInstance = Instantiate(rocket, new Vector2(transform.position.x + 1.5f, transform.position.y - 0.2f), Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
            bulletInstance.velocity = new Vector2(BulletSpeed, 0); //Debug.Log("Shoot");
        }
    }


    void Death()
    {
        dead = true;
        live = false;
        anim.SetTrigger("Death");
        gameObject.tag = "Untagged";
    }



}
