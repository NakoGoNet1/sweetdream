﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YouLooseScript : MonoBehaviour
{

    public GameObject Player1;
    public GameObject Player2;
    public GameObject Added5;
    public int SortLayer = 0;
    public int FrameRate = 0;
    private bool isYouLoose;
    private int AddLife = 1;
    Animator anim;

    //public int SortingLayerID = SortingLayer.GetLayerValueFromName("Default");
    // Use this for initialization
    void Start()
    {
        

        anim = GetComponent<Animator>();

        Renderer renderer = this.gameObject.GetComponent<Renderer>();
        Renderer renderer2 = Added5.gameObject.GetComponent<Renderer>();
        if (renderer != null)
        {
            renderer.sortingOrder = SortLayer;
            renderer2.sortingOrder = SortLayer;
            //renderer.sortingLayerID = SortingLayerID;
        }
    }

    private void Update()
    {

        Application.targetFrameRate = FrameRate;

        if (Player1.GetComponent<Mario1Controller2DScript>().Dead == true && Player2.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            if (isYouLoose == false)
            {
                anim.SetTrigger("YouLoose");
                isYouLoose = true;
            }
            
        }

        if (Input.GetKeyUp(KeyCode.Y))
        {
            anim.SetTrigger("Added5Life");
            Player1.GetComponent<Mario1Controller2DScript>().Life = Player1.GetComponent<Mario1Controller2DScript>().Life + AddLife;
            Player2.GetComponent<Mario1Controller2DScript>().Life = Player2.GetComponent<Mario1Controller2DScript>().Life + AddLife;
        }
    }

    private void FixedUpdate()
    {
        //Debug.Log(AudioSettings.dspTime);
    }

}
