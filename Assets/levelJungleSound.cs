﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelJungleSound : MonoBehaviour {



    public GameObject Player1;
    Animator anim;
    private string animState = "Idle";
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {

        if (Player1.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            if (animState != "end")
            {
                anim.SetTrigger("end");
                animState = "end";
            }
        }


    }
}
