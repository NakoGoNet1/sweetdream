﻿using UnityEngine;
using System.Collections;

public class MonkeyOnLian : MonoBehaviour
{
    public Transform target;
    public GameObject ammo;
    public GameObject Player;
    public int bulletsPerSec = 10;
    public float shootPauseDuration = 3.0f;
    public float createPauseDuration = 2f;
    public float force = 5.0f;
    int bullets = 0;
    bool pause = false;
    bool pauseDead = false;
    public GameObject LeftBorder;
    public GameObject RightBorder;
    private bool visible = false;
    private bool dead = true;
    //private bool live = false;
    private bool canShoot = false;
    public int HP = 2;
    Animator anim;

    void OnDrawGizmosSelected()
    {
        
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, new Vector3(LeftBorder.transform.position.x, LeftBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(RightBorder.transform.position.x, RightBorder.transform.position.y, 0.0f));
        
    }

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

            //Gizmos.color = Color.blue;
            //Gizmos.DrawLine(new Vector3(LeftBorder.transform.position.x, LeftBorder.transform.position.y, LeftBorder.transform.position.z), new Vector3(RightBorder.transform.position.x, RightBorder.transform.position.y, RightBorder.transform.position.z));
            if (dead == true)
        {
            Create();
        }
        else
        {
            Shoot();
            CheckEnemyHP();
        }

        
    }


    private void CheckEnemyHP()
    {
        if (GetComponent<EnemyHealth>().HP_enemy <= 0 && dead == false)//если у врага нету жизней убиваем его
        {
            anim.SetTrigger("Die");
            dead = true; canShoot = false; GetComponent<EnemyHealth>().HP_enemy = HP;//live = false; 
        }
    }

    private void Create()
    {
        Visible();
        if (visible == true)
        {
            //live = true;
            if (pauseDead == false)
            {
                pauseDead = true;
                StartCoroutine("PauseCreate");
            }
        }
    }

    private void CanShoot()
    {
        canShoot = true;
    }

    IEnumerator PauseCreate()
    {
        yield return new WaitForSeconds(createPauseDuration);
        dead = false;
        pauseDead = false;
        anim.SetTrigger("Create");
    }

    private void Visible()
    {
        //Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (Player.transform.position.x > LeftBorder.transform.position.x && Player.transform.position.x < RightBorder.transform.position.x)
        {
            visible = true;
        }
        else
        {
            visible = false;
        }
    }

    void Shoot()
    {

        Visible();
        if (bullets < bulletsPerSec && visible == true && canShoot == true)
        {
            bullets++;
            GameObject bullet = Instantiate(ammo, transform.position,transform.rotation) as GameObject;
            bullet.GetComponent<Rigidbody2D>().AddForce((target.position - transform.position).normalized * force,ForceMode2D.Impulse);
            bullet.transform.position = new Vector3(bullet.transform.position.x, bullet.transform.position.y, 0);
            //Destroy(bullet, 3.0f);
            anim.SetTrigger("Fire");
        }
        else if (pause == false)
        {
            pause = true;
            StartCoroutine("ShootPause");
        }
    }

    IEnumerator ShootPause()
    {
        yield return new WaitForSeconds(shootPauseDuration);
        bullets = 0;
        pause = false;
    }
}