﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayersHeart : MonoBehaviour {




    public GameObject Player1;
    public GameObject Player2;

    public GameObject PlayerName_1;
    public GameObject PlayerPressAnyKey_1;
    public GameObject PlayerHearth1_1;
    public GameObject PlayerHearth2_1;
    public GameObject PlayerHearth3_1;
    public GameObject PlayerHearth4_1;
    public GameObject PlayerHearth5_1;
    public GameObject PlayerHearth6_1;
    public GameObject PlayerSquirrel1_1;
    public GameObject PlayerSquirrel2_1;
    public GameObject PlayerSquirrel3_1;
    public GameObject PlayerSquirrel4_1;
    public GameObject PlayerSquirrel5_1;
    public GameObject PlayerSquirrel6_1;

    public GameObject PlayerName_2;
    public GameObject PlayerPressAnyKey_2;
    public GameObject PlayerHearth1_2;
    public GameObject PlayerHearth2_2;
    public GameObject PlayerHearth3_2;
    public GameObject PlayerHearth4_2;
    public GameObject PlayerHearth5_2;
    public GameObject PlayerHearth6_2;
    public GameObject PlayerSquirrel1_2;
    public GameObject PlayerSquirrel2_2;
    public GameObject PlayerSquirrel3_2;
    public GameObject PlayerSquirrel4_2;
    public GameObject PlayerSquirrel5_2;
    public GameObject PlayerSquirrel6_2;

    // Use this for initialization
    void Start () {
		
	}

    private void PlayerHearth_1()
    {
        if (Player1.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            PlayerName_1.GetComponent<Image>().enabled = false;
            PlayerHearth1_1.GetComponent<Image>().enabled = false;
            PlayerHearth2_1.GetComponent<Image>().enabled = false;
            PlayerHearth3_1.GetComponent<Image>().enabled = false;
            PlayerHearth4_1.GetComponent<Image>().enabled = false;
            PlayerHearth5_1.GetComponent<Image>().enabled = false;
            PlayerHearth6_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel1_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel2_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel3_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel4_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel5_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel6_1.GetComponent<Image>().enabled = false;
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Dead == false && Player1.GetComponent<Mario1Controller2DScript>().Life == 1)
        {
            PlayerName_1.GetComponent<Image>().enabled = true;
            PlayerHearth1_1.GetComponent<Image>().enabled = true;
            PlayerHearth2_1.GetComponent<Image>().enabled = false;
            PlayerHearth3_1.GetComponent<Image>().enabled = false;
            PlayerHearth4_1.GetComponent<Image>().enabled = false;
            PlayerHearth5_1.GetComponent<Image>().enabled = false;
            PlayerHearth6_1.GetComponent<Image>().enabled = false;
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Dead == false && Player1.GetComponent<Mario1Controller2DScript>().Life == 2)
        {
            PlayerName_1.GetComponent<Image>().enabled = true;
            PlayerHearth1_1.GetComponent<Image>().enabled = true;
            PlayerHearth2_1.GetComponent<Image>().enabled = true;
            PlayerHearth3_1.GetComponent<Image>().enabled = false;
            PlayerHearth4_1.GetComponent<Image>().enabled = false;
            PlayerHearth5_1.GetComponent<Image>().enabled = false;
            PlayerHearth6_1.GetComponent<Image>().enabled = false;
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Dead == false && Player1.GetComponent<Mario1Controller2DScript>().Life == 3)
        {
            PlayerName_1.GetComponent<Image>().enabled = true;
            PlayerHearth1_1.GetComponent<Image>().enabled = true;
            PlayerHearth2_1.GetComponent<Image>().enabled = true;
            PlayerHearth3_1.GetComponent<Image>().enabled = true;
            PlayerHearth4_1.GetComponent<Image>().enabled = false;
            PlayerHearth5_1.GetComponent<Image>().enabled = false;
            PlayerHearth6_1.GetComponent<Image>().enabled = false;
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Dead == false && Player1.GetComponent<Mario1Controller2DScript>().Life == 4)
        {
            PlayerName_1.GetComponent<Image>().enabled = true;
            PlayerHearth1_1.GetComponent<Image>().enabled = true;
            PlayerHearth2_1.GetComponent<Image>().enabled = true;
            PlayerHearth3_1.GetComponent<Image>().enabled = true;
            PlayerHearth4_1.GetComponent<Image>().enabled = true;
            PlayerHearth5_1.GetComponent<Image>().enabled = false;
            PlayerHearth6_1.GetComponent<Image>().enabled = false;
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Dead == false && Player1.GetComponent<Mario1Controller2DScript>().Life == 5)
        {
            PlayerName_1.GetComponent<Image>().enabled = true;
            PlayerHearth1_1.GetComponent<Image>().enabled = true;
            PlayerHearth2_1.GetComponent<Image>().enabled = true;
            PlayerHearth3_1.GetComponent<Image>().enabled = true;
            PlayerHearth4_1.GetComponent<Image>().enabled = true;
            PlayerHearth5_1.GetComponent<Image>().enabled = true;
            PlayerHearth6_1.GetComponent<Image>().enabled = false;
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Dead == false && Player1.GetComponent<Mario1Controller2DScript>().Life > 5)
        {
            PlayerName_1.GetComponent<Image>().enabled = true;
            PlayerHearth1_1.GetComponent<Image>().enabled = true;
            PlayerHearth2_1.GetComponent<Image>().enabled = true;
            PlayerHearth3_1.GetComponent<Image>().enabled = true;
            PlayerHearth4_1.GetComponent<Image>().enabled = true;
            PlayerHearth5_1.GetComponent<Image>().enabled = true;
            PlayerHearth6_1.GetComponent<Image>().enabled = true;
        }
        if (Player1.GetComponent<Mario1Controller2DScript>().Belka == 0)
        {
            PlayerSquirrel1_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel2_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel3_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel4_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel5_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel6_1.GetComponent<Image>().enabled = false;
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Belka == 1)
        {
            PlayerSquirrel1_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel2_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel3_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel4_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel5_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel6_1.GetComponent<Image>().enabled = false;
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Belka == 2)
        {
            PlayerSquirrel1_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel2_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel3_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel4_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel5_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel6_1.GetComponent<Image>().enabled = false;
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Belka == 3)
        {
            PlayerSquirrel1_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel2_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel3_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel4_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel5_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel6_1.GetComponent<Image>().enabled = false;
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Belka == 4)
        {
            PlayerSquirrel1_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel2_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel3_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel4_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel5_1.GetComponent<Image>().enabled = false;
            PlayerSquirrel6_1.GetComponent<Image>().enabled = false;
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Belka == 5)
        {
            PlayerSquirrel1_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel2_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel3_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel4_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel5_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel6_1.GetComponent<Image>().enabled = false;
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Belka > 5)
        {
            PlayerSquirrel1_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel2_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel3_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel4_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel5_1.GetComponent<Image>().enabled = true;
            PlayerSquirrel6_1.GetComponent<Image>().enabled = true;
        }

        if (Player1.GetComponent<Mario1Controller2DScript>().Dead == true && Player2.GetComponent<Mario1Controller2DScript>().Life > 1)
        {
            PlayerPressAnyKey_1.GetComponent<Image>().enabled = true;
        }
        else
        {
            PlayerPressAnyKey_1.GetComponent<Image>().enabled = false;
        }
    }


    private void PlayerHearth_2()
    {
        if (Player2.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            PlayerName_2.GetComponent<Image>().enabled = false;
            PlayerHearth1_2.GetComponent<Image>().enabled = false;
            PlayerHearth2_2.GetComponent<Image>().enabled = false;
            PlayerHearth3_2.GetComponent<Image>().enabled = false;
            PlayerHearth4_2.GetComponent<Image>().enabled = false;
            PlayerHearth5_2.GetComponent<Image>().enabled = false;
            PlayerHearth6_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel1_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel2_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel3_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel4_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel5_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel6_2.GetComponent<Image>().enabled = false;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Dead == false && Player2.GetComponent<Mario1Controller2DScript>().Life == 1)
        {
            PlayerName_2.GetComponent<Image>().enabled = true;
            PlayerHearth1_2.GetComponent<Image>().enabled = true;
            PlayerHearth2_2.GetComponent<Image>().enabled = false;
            PlayerHearth3_2.GetComponent<Image>().enabled = false;
            PlayerHearth4_2.GetComponent<Image>().enabled = false;
            PlayerHearth5_2.GetComponent<Image>().enabled = false;
            PlayerHearth6_2.GetComponent<Image>().enabled = false;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Dead == false && Player2.GetComponent<Mario1Controller2DScript>().Life == 2)
        {
            PlayerName_2.GetComponent<Image>().enabled = true;
            PlayerHearth1_2.GetComponent<Image>().enabled = true;
            PlayerHearth2_2.GetComponent<Image>().enabled = true;
            PlayerHearth3_2.GetComponent<Image>().enabled = false;
            PlayerHearth4_2.GetComponent<Image>().enabled = false;
            PlayerHearth5_2.GetComponent<Image>().enabled = false;
            PlayerHearth6_2.GetComponent<Image>().enabled = false;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Dead == false && Player2.GetComponent<Mario1Controller2DScript>().Life == 3)
        {
            PlayerName_2.GetComponent<Image>().enabled = true;
            PlayerHearth1_2.GetComponent<Image>().enabled = true;
            PlayerHearth2_2.GetComponent<Image>().enabled = true;
            PlayerHearth3_2.GetComponent<Image>().enabled = true;
            PlayerHearth4_2.GetComponent<Image>().enabled = false;
            PlayerHearth5_2.GetComponent<Image>().enabled = false;
            PlayerHearth6_2.GetComponent<Image>().enabled = false;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Dead == false && Player2.GetComponent<Mario1Controller2DScript>().Life == 4)
        {
            PlayerName_2.GetComponent<Image>().enabled = true;
            PlayerHearth1_2.GetComponent<Image>().enabled = true;
            PlayerHearth2_2.GetComponent<Image>().enabled = true;
            PlayerHearth3_2.GetComponent<Image>().enabled = true;
            PlayerHearth4_2.GetComponent<Image>().enabled = true;
            PlayerHearth5_2.GetComponent<Image>().enabled = false;
            PlayerHearth6_2.GetComponent<Image>().enabled = false;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Dead == false && Player2.GetComponent<Mario1Controller2DScript>().Life == 5)
        {
            PlayerName_2.GetComponent<Image>().enabled = true;
            PlayerHearth1_2.GetComponent<Image>().enabled = true;
            PlayerHearth2_2.GetComponent<Image>().enabled = true;
            PlayerHearth3_2.GetComponent<Image>().enabled = true;
            PlayerHearth4_2.GetComponent<Image>().enabled = true;
            PlayerHearth5_2.GetComponent<Image>().enabled = true;
            PlayerHearth6_2.GetComponent<Image>().enabled = false;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Dead == false && Player2.GetComponent<Mario1Controller2DScript>().Life > 5)
        {
            PlayerName_2.GetComponent<Image>().enabled = true;
            PlayerHearth1_2.GetComponent<Image>().enabled = true;
            PlayerHearth2_2.GetComponent<Image>().enabled = true;
            PlayerHearth3_2.GetComponent<Image>().enabled = true;
            PlayerHearth4_2.GetComponent<Image>().enabled = true;
            PlayerHearth5_2.GetComponent<Image>().enabled = true;
            PlayerHearth6_2.GetComponent<Image>().enabled = true;
        }
        if (Player2.GetComponent<Mario1Controller2DScript>().Belka == 0)
        {
            PlayerSquirrel1_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel2_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel3_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel4_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel5_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel6_2.GetComponent<Image>().enabled = false;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Belka == 1)
        {
            PlayerSquirrel1_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel2_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel3_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel4_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel5_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel6_2.GetComponent<Image>().enabled = false;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Belka == 2)
        {
            PlayerSquirrel1_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel2_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel3_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel4_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel5_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel6_2.GetComponent<Image>().enabled = false;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Belka == 3)
        {
            PlayerSquirrel1_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel2_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel3_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel4_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel5_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel6_2.GetComponent<Image>().enabled = false;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Belka == 4)
        {
            PlayerSquirrel1_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel2_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel3_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel4_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel5_2.GetComponent<Image>().enabled = false;
            PlayerSquirrel6_2.GetComponent<Image>().enabled = false;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Belka == 5)
        {
            PlayerSquirrel1_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel2_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel3_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel4_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel5_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel6_2.GetComponent<Image>().enabled = false;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Belka > 5)
        {
            PlayerSquirrel1_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel2_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel3_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel4_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel5_2.GetComponent<Image>().enabled = true;
            PlayerSquirrel6_2.GetComponent<Image>().enabled = true;
        }

        if (Player2.GetComponent<Mario1Controller2DScript>().Dead == true && Player1.GetComponent<Mario1Controller2DScript>().Life > 1)
        {
            PlayerPressAnyKey_2.GetComponent<Image>().enabled = true;
        }
        else
        {
            PlayerPressAnyKey_2.GetComponent<Image>().enabled = false;
        }
    }

    // Update is called once per frame
    void Update () {

        PlayerHearth_1();
        PlayerHearth_2();


    }
}
