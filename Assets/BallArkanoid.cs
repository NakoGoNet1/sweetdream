﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallArkanoid : MonoBehaviour
{
    Rigidbody2D rb;
    public GameObject Player;
    public float Speed = 0.2f;
    public float x = 0.2f;
    public float y = 0.2f;
    public int RocketPower;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.AddForce(transform.up * Speed, ForceMode2D.Impulse);
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<CircleCollider2D>(), Player.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока

    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        //Debug.Log(Input.GetAxisRaw("Horizontal"));
        //transform.position = new Vector2(transform.position.x + x * Speed, transform.position.y + y * Speed);
    }

    private void OnTriggerEnter2D(Collider2D collision)//обработка столкновений
    {
        if (collision.gameObject.tag == "Player")//если мы наткнулись на игрока
        {
            rb.velocity = Vector2.zero;
            //rb.AddForce(transform.up * 10f, ForceMode2D.Impulse);
            if (collision.gameObject.GetComponent<Rigidbody2D>().velocity.x > Speed)
            {
                rb.velocity = new Vector2(Speed, Speed);
            }
            else
            if (collision.gameObject.GetComponent<Rigidbody2D>().velocity.x < -Speed)
            {
                rb.velocity = new Vector2(-Speed, Speed);
            }
            else
            if (collision.gameObject.GetComponent<Rigidbody2D>().velocity.x == 0)
            {
                
                if (transform.position.x > collision.gameObject.transform.position.x)
                {
                    Debug.Log("!!!!!!!!!!!!!!"+ (transform.position.x - collision.gameObject.transform.position.x));
                    rb.velocity = new Vector2((transform.position.x - collision.gameObject.transform.position.x)*(Speed*2), Speed);
                }
                else
                {
                    rb.velocity = new Vector2((transform.position.x - collision.gameObject.transform.position.x) * (Speed * 2), Speed);
                }
            }
            else
            {
                rb.velocity = new Vector2(collision.gameObject.GetComponent<Rigidbody2D>().velocity.x, Speed);
            }

        }

        if (collision.tag == "Floor")
        {

            Player.GetComponent<Mario1Controller2DScript>().Damage(1, false);

        }

    }

    void OnCollisionEnter2D(Collision2D collision)//если игрок наткнулся на врага по триггеру
    {
        if (collision.gameObject.tag == "Enemy")
        {

            collision.gameObject.GetComponent<EnemyHealth>().Hurt(RocketPower);
            collision.gameObject.GetComponent<EnemyHealth>().Hurt(RocketPower);
            collision.gameObject.GetComponent<EnemyHealth>().Hurt(RocketPower);
            collision.gameObject.GetComponent<EnemyHealth>().Hurt(RocketPower);
            collision.gameObject.GetComponent<EnemyHealth>().Hurt(RocketPower);
            collision.gameObject.GetComponent<EnemyHealth>().Hurt(RocketPower);
            collision.gameObject.GetComponent<EnemyHealth>().Hurt(RocketPower);
            collision.gameObject.GetComponent<EnemyHealth>().Hurt(RocketPower);
            collision.gameObject.GetComponent<EnemyHealth>().Hurt(RocketPower);
            collision.gameObject.GetComponent<EnemyHealth>().Hurt(RocketPower);
        }
    }
}
