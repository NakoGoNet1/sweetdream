﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelJungle_Eym : MonoBehaviour {


    Animator anim;
    Rigidbody2D rb;
    public GameObject Player1;
    public GameObject Player2;
    public float jumpTime;
    public float startTime;
    public float moveSpeed = 1f;
    private float dividerMove = 15f;
    //private bool isJump = false; 
    //private string animState = "Idle";
    public int HP = 2;                  // How many times the enemy can be hit before it dies.
    private bool run = false;
    //private bool runStop = false;
    //private bool runEnd = false;
    public bool side = true; //с какой стороны игрок, в какую бежать врагу при создании (true = право false - лево)
    public float HightEnemyJump = 5550f;
    public bool start = false;
    public bool live = false;
    public bool startRun = false;

    // Use this for initialization
    void Start () {

        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player2.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока

    }


    private void CheckEnemyHP()
    {
        if (GetComponent<EnemyHealth>().HP_enemy <= 0)//если у врага нету жизней убиваем его
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update () {

        CheckEnemyHP();

    }

    public void StartRun()
    {
        run = true;
        anim.SetTrigger("Run");
    }

    private void FixedUpdate()
    {
        if (run == true)
        {
            //Debug.Log("!!!!!!!!!!!!!!!");

            if (side == true)
            {

                transform.localRotation = Quaternion.Euler(0, 180, 0);
                rb.velocity = new Vector2(transform.localScale.x * moveSpeed / dividerMove, GetComponent<Rigidbody2D>().velocity.y);
                if (dividerMove <= 1f)
                {
                    dividerMove = 1f;
                }
                else
                {
                    dividerMove = dividerMove / 1.06f;
                }

            }
            else
            {

                transform.localRotation = Quaternion.Euler(0, 0, 0);
                rb.velocity = new Vector2(transform.localScale.x * (-moveSpeed / dividerMove), GetComponent<Rigidbody2D>().velocity.y);
                if (dividerMove <= 1f)
                {
                    dividerMove = 1f;
                }
                else
                {
                    dividerMove = dividerMove / 1.06f;
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)//обработка столкновений
    {
        if (collision.gameObject.tag == "Player")//если мы наткнулись на игрока
        {
            collision.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, false);//приносим ему урон
        }

        if (collision.gameObject.tag == "EnemyReturnColl")//если мы наткнулись на коллайдер подпрыга врага
        {
            Destroy(gameObject);
        }



    }
}
