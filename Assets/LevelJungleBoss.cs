﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelJungleBoss : MonoBehaviour
{



    public GameObject Player1; //подключаем игрока для понимания с какой он стороны для поворота врага
    public GameObject Player2;
    public GameObject AggrePlayer;
    public GameObject PlayerCenter;
    public GameObject Spider1;
    public GameObject Rocket;
    public Rigidbody2D Enemy1;
    public Rigidbody2D Enemy2;
    public Rigidbody2D Rockets;
    public Rigidbody2D Corgy;
    public GameObject Spider1live;
    public GameObject Spider1PointBirth;
    public GameObject Spider2PointBirth;
    public GameObject Spider3PointBirth;
    public GameObject Spider1PointDead;
    public GameObject PlayerRocket;
    public GameObject CenterScreen;
    public int HP_phase1;
    public int HP_phase2;
    public int HP_phase3;
    public int HP_phase4;
    public int HP = 2;                  // How many times the enemy can be hit before it dies.
    public float moveSpeed = 1f;
    public float moveSpeed1 = 1f;
    public float moveSpeed2 = 1f;
    public float moveSpeed3 = 1f;
    public float moveSpeed4 = 1f;
    public float phase4JumpPower = 1f;
    public GameObject MainImage;
    Animator anim;
    Rigidbody2D rb;
    public int phase = 1;
    //0 - босс падает сверху, стоит секунду,
    //1-босс начинает бежать с 1-й скоростью,******
    //2 - босс останавливается и начинает прыгать,
    //3 - босс начинает бежать с 2-й скорость,*****
    //4 - начинает прыгать
    //5 - босс начинает бежать с 3-й скорость,*****
    //6 - помирает
    private float dividerMove = 15f;
    public bool side = true; //с какой стороны игрок, в какую бежать врагу при создании (true = право false - лево)
    public float HightEnemyJump = 5550f;
    public float startTime = 1f;
    public float startEnemy = 1f;
    public bool cameraShake = false;
    // How long the object should shake for.
    public float shakeDuration = 0f;
    //private bool enemy1Live = false;
    public bool startAnim = false;
    public bool endAnim = false;
    //private bool phase4JumpPower_trigger = false;
    private bool phase135jump = false;
    private int Enemy2trigger = 0;
    //private bool WallTouch = false;
    public int CorgyCount = 0;
    public int MaxCorgy = 1;
    // Amplitude of the shake. A larger value shakes the camera harder.

    // Use this for initialization
    void Start()
    {

        //StartCoroutine("StartRun");

        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        //rb.simulated = false;
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player2.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока

        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), PlayerRocket.gameObject.GetComponent<BoxCollider2D>(), true);
    }



    private void OnGUI()
    {
        {
            GUI.Box(new Rect(0, 0, 100, 30), "Life = " + GetComponent<EnemyHealth>().HP_enemy);
            //GUI.Box(new Rect(150, 0, 100, 30), "Belka = " + Belka);

        }
    }

    // Update is called once per frame
    void Update()
    {

        //Debug.Log(phase);

        if (phase == 0 && startAnim == false)
        {
            //gameObject.tag = "Untagged";
            anim.SetTrigger("phase_0");
            startAnim = true;
            endAnim = false;
        }

        if (phase == 0 && startAnim == true && endAnim == true)//переход на фазу - 1*******
        {
            moveSpeed = moveSpeed1;
            phase = 1;
            startAnim = false;
            endAnim = false;
            anim.SetTrigger("phase_1");
        }

        if (phase == 1 && GetComponent<EnemyHealth>().HP_enemy <= HP_phase1 && startAnim == false)//переход на фазу - 2
        {
            //this.GetComponent<EnemyHealth>().HP_enemy = HP_phase1;
            //gameObject.tag = "Untagged";
            rb.velocity = Vector2.zero;
            anim.SetTrigger("phase_2");
            phase = 2;
            startAnim = true;
            endAnim = false;
        }

        if (phase == 2 && startAnim == true && endAnim == true)//переход на фазу - 3*******
        {
            //gameObject.tag = "Enemy";
            dividerMove = 15f;
            moveSpeed = moveSpeed2;
            phase = 3;
            startAnim = false;
            endAnim = false;
            anim.SetTrigger("phase_1");
        }

        if (phase == 3 && GetComponent<EnemyHealth>().HP_enemy <= HP_phase2 && startAnim == false)//переход на фазу - 4
        {

            //this.GetComponent<EnemyHealth>().HP_enemy = HP_phase2;
            //gameObject.tag = "Untagged";
            rb.velocity = Vector2.zero;
            anim.SetTrigger("phase_2");
            phase = 4;
            startAnim = true;
            endAnim = false;
        }

        if (phase == 4 && startAnim == true && endAnim == true)//переход на фазу - 5*******
        {
            //gameObject.tag = "Enemy";
            dividerMove = 15f;
            moveSpeed = moveSpeed3;
            phase = 5;
            startAnim = false;
            endAnim = false;
            anim.SetTrigger("phase_1");
        }

        if (phase == 5 && GetComponent<EnemyHealth>().HP_enemy <= HP_phase3 && startAnim == false)//переход на фазу - 6
        {
            //this.GetComponent<EnemyHealth>().HP_enemy = HP_phase2;
            //gameObject.tag = "Untagged";
            rb.velocity = Vector2.zero;
            anim.SetTrigger("phase_3");
            phase = 6;
            startAnim = true;
            endAnim = false;
            //rb.bodyType = RigidbodyType2D.Static;
        }

        if (phase == 6 && startAnim == true && endAnim == true)//переход на фазу - 7*******прыганья
        {
            //gameObject.tag = "Enemy";

            //rb.bodyType = RigidbodyType2D.Static;

            dividerMove = 15f;
            moveSpeed = moveSpeed4;
            phase = 7;
            startAnim = false;
            endAnim = false;
            anim.SetTrigger("phase_4");
            //Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<CapsuleCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
            //Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Rocket.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока


            //GetComponent<BoxCollider2D>().enabled = false;

        }

        if (phase == 7 && GetComponent<EnemyHealth>().HP_enemy <= HP_phase4 && startAnim == false && transform.position.x < Spider1PointBirth.transform.position.x + 0.2f &&
            phase == 7 && GetComponent<EnemyHealth>().HP_enemy <= HP_phase4 && startAnim == false && transform.position.x > Spider1PointBirth.transform.position.x - 0.2f ||
            phase == 7 && GetComponent<EnemyHealth>().HP_enemy <= HP_phase4 && startAnim == false && transform.position.x < Spider3PointBirth.transform.position.x + 0.2f &&
            phase == 7 && GetComponent<EnemyHealth>().HP_enemy <= HP_phase4 && startAnim == false && transform.position.x > Spider3PointBirth.transform.position.x - 0.2f)//переход на фазу - 8 - увеличение
        {
            //this.GetComponent<EnemyHealth>().HP_enemy = HP_phase2;
            //gameObject.tag = "Untagged";
            rb.velocity = Vector2.zero;
            //anim.SetTrigger("phase_3");
            phase = 8;
            startAnim = true;
            endAnim = false;
            anim.SetTrigger("phase_5");

            if (CenterScreen.transform.position.x < transform.position.x)//ударился справа в стенку
            {
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                side = false;
            }

            else
            {
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                side = true;
            }
            //rb.bodyType = RigidbodyType2D.Static;
        }

        if (phase == 8 && startAnim == true && endAnim == true)//переход на фазу - 9*******большой робот
        {
            //gameObject.tag = "Enemy";

            //rb.bodyType = RigidbodyType2D.Static;

            Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
            Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<CapsuleCollider2D>(), true);//исключаем колайдер врага и колайдер игрока

            dividerMove = 15f;
            moveSpeed = moveSpeed4;
            phase = 9;
            startAnim = false;
            endAnim = false;
            anim.SetTrigger("phase_6");



            //Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<CapsuleCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
            //Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Rocket.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока


            //GetComponent<BoxCollider2D>().enabled = false;

        }



        CheckEnemyHP();

    }


    IEnumerator StartRun()
    {

        yield return new WaitForSeconds(startTime);

        phase = 2;
    }

    IEnumerator StartEnemy()
    {

        yield return new WaitForSeconds(startEnemy);

        //enemy1Live = false;
    }


    IEnumerator StopCameraShaking()
    {

        yield return new WaitForSeconds(shakeDuration);

        cameraShake = false;
        PlayerCenter.gameObject.GetComponent<PlayerCenter>().shake_x = 0f;
    }

    void cameraShaking()
    {
        //PlayerCenter.gameObject.GetComponent<PlayerCenter>().shake_x = Random.Range(-2.0f, 2.0f);
    }

    private void CheckEnemyHP()
    {
        if (GetComponent<EnemyHealth>().HP_enemy <= 0)//если у врага нету жизней убиваем его
        {
            Destroy(gameObject);
        }
    }


    public void EndAnim()
    {

        endAnim = true;
    }

    public void RocketsStart()
    {

        Rigidbody2D enemy = Instantiate(Rockets, new Vector2(transform.position.x + 100f, transform.position.y), Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
        enemy.GetComponent<LevelJungleBoss_rocket>().phase = 0;
        //enemy.GetComponent<Rigidbody2D>().simulated = true;
        //enemy.GetComponent<LevelJungle_Eym>().side = true;
        //enemy.AddForce(new Vector2(4, 16), ForceMode2D.Impulse);
    }

    public void CorgyStart()
    {
        //Instantiate(Corgy, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);

        if (CorgyCount < MaxCorgy - 1)
        {
            Rigidbody2D enemy = Instantiate(Corgy, new Vector2(transform.position.x, transform.position.y + 2f), Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;

            //enemy.GetComponent<Rigidbody2D>().simulated = true;
            enemy.GetComponent<LevelJungle_BossCorgy>().side = side;
            enemy.GetComponent<LevelJungle_BossCorgy>().live = true;
            CorgyCount++;

            //enemy.GetComponent<Rigidbody2D>().simulated = true;
            //enemy.GetComponent<LevelJungle_Eym>().side = true;
            //enemy.AddForce(new Vector2(4, 16), ForceMode2D.Impulse);
        }

    }

    public void CreateEnemy2()//в анимации прыганья босса создаем разлетающихся врагов
    {
        if (Enemy2trigger == 2)//появление врагов через два прыжка
        {
            Rigidbody2D enemy = Instantiate(Enemy2, new Vector2(transform.position.x, transform.position.y), Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
            enemy.GetComponent<Rigidbody2D>().simulated = true;
            enemy.GetComponent<LevelJungle_Eym>().side = true;
            enemy.AddForce(new Vector2(4, 16), ForceMode2D.Impulse);

            Rigidbody2D enemy2 = Instantiate(Enemy2, new Vector2(transform.position.x, transform.position.y), Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
            enemy2.GetComponent<Rigidbody2D>().simulated = true;
            enemy2.GetComponent<LevelJungle_Eym>().side = false;
            enemy2.AddForce(new Vector2(-4, 16), ForceMode2D.Impulse);

            Enemy2trigger = 1;
        }
        else
        {
            Enemy2trigger++;
        }

        //if (Player1.transform.position.x - transform.position.x < 2.3 && Player1.transform.position.x - transform.position.x > -2.3)//появление врагов если игрок находится под боссом в момент прыжка
        //{
        //        Rigidbody2D enemy = Instantiate(Enemy2, new Vector2(transform.position.x, transform.position.y), Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
        //        enemy.GetComponent<Rigidbody2D>().simulated = true;
        //        enemy.GetComponent<LevelJungle_Eym>().side = true;
        //        enemy.AddForce(new Vector2(4, 16), ForceMode2D.Impulse);

        //        Rigidbody2D enemy2 = Instantiate(Enemy2, new Vector2(transform.position.x, transform.position.y), Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
        //        enemy2.GetComponent<Rigidbody2D>().simulated = true;
        //        enemy2.GetComponent<LevelJungle_Eym>().side = false;
        //        enemy2.AddForce(new Vector2(-4, 16), ForceMode2D.Impulse);
        //}


    }


    public void Phase4Jump()
    {
        rb.AddForce(transform.up * 2f * phase4JumpPower, ForceMode2D.Impulse);//добавляем вектор подпрыга
    }

    public void Phase123Jump_exit()
    {

        phase135jump = false;
        if (phase == 1 || phase == 3 || phase == 5)
        {
            anim.SetTrigger("phase_1");
        }
    }



    private void FixedUpdate()
    {
        //Debug.Log("la la la " + System.Math.Round(Player1.transform.position.x, 1) + "la la la " + System.Math.Round(transform.position.x, 1));


        if (cameraShake == true)
        {
            cameraShaking();
        }
        if (phase == 1 || phase == 3 || phase == 5 || phase == 7)
        {

            if (phase == 1 || phase == 3 || phase == 5)//если фаза 1 - бег по низу
            {
                if (Player1.gameObject.GetComponent<Mario1Controller2DScript>().isGrounded == false)//и игрок в прыжке
                {

                    if (side == true && phase135jump == false && Player1.transform.position.x - transform.position.x < 2.5 && Player1.transform.position.x - transform.position.x > 2.2)//если босс бежит вправо
                    {
                        phase135jump = true;
                        rb.velocity = Vector2.zero;
                        rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
                        if (phase == 1 || phase == 3 || phase == 5)//что бы не прыгал и не глючил при переходе на следующую фазу
                        {
                            anim.SetTrigger("phase_135jump");
                        }

                    }
                    else if (side == false && phase135jump == false && transform.position.x - Player1.transform.position.x < 2.5 && transform.position.x - Player1.transform.position.x > 2.2)//если босс бежит вправо
                    {
                        phase135jump = true;
                        rb.velocity = Vector2.zero;
                        rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
                        if (phase == 1 || phase == 3 || phase == 5)//что бы не прыгал и не глючил при переходе на следующую фазу
                        {
                            anim.SetTrigger("phase_135jump");
                        }
                    }


                    //if (System.Math.Round(Player1.transform.position.x, 1) == System.Math.Round(transform.position.x, 1))// и игрок находится над боссом

                    //{
                    //    rb.velocity = Vector2.zero;
                    //    rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
                    //}

                }
            }


            if (Spider1live.GetComponent<EnemyHealth>().HP_enemy <= 0 && Spider1live.GetComponent<EnemySpider>().dead == false)//если у врага нету жизней убиваем его
            {
                //Spider1.transform.position = Spider1PointDead.transform.position;
            }

            if (side == true)
            {

                transform.localRotation = Quaternion.Euler(0, 180, 0);
                rb.velocity = new Vector2(transform.localScale.x * moveSpeed / dividerMove, GetComponent<Rigidbody2D>().velocity.y);
                if (dividerMove <= 1f)
                {
                    dividerMove = 1f;
                }
                else
                {
                    dividerMove = dividerMove / 1.06f;
                }

            }
            else
            {

                transform.localRotation = Quaternion.Euler(0, 0, 0);
                rb.velocity = new Vector2(transform.localScale.x * (-moveSpeed / dividerMove), GetComponent<Rigidbody2D>().velocity.y);
                if (dividerMove <= 1f)
                {
                    dividerMove = 1f;
                }
                else
                {
                    dividerMove = dividerMove / 1.06f;
                }
            }

        }


    }

    private void OnTriggerEnter2D(Collider2D collision)//обработка столкновений
    {
        if (collision.gameObject.tag == "Player")//если мы наткнулись на игрока
        {
            if (phase == 1 || phase == 3 || phase == 5 || phase == 7 || phase == 9)
            {
                collision.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, false);//приносим ему урон
            }

        }
        if (collision.gameObject.tag == "EnemyReturnColl")//если мы наткнулись на коллайдер розворота врага
        {


            //WallTouch = true;

            cameraShake = true;
            StartCoroutine("StopCameraShaking");

            //if (phase == 2 && enemy1Live == false)
            if (phase == 1 || phase == 3 || phase == 5)
            {
                //enemy1Live = true;
                StartCoroutine("StartEnemy");

                Rigidbody2D enemy = Instantiate(Enemy1, new Vector2(Player1.transform.position.x, Spider1PointBirth.transform.position.y), Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
                enemy.GetComponent<LevelJungle_Slug>().start = true;
                enemy.GetComponent<LevelJungle_Slug>().side = (Random.value > 0.5f);
            }

            //rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга при ударе об стену

            if (phase == 1 || phase == 3 || phase == 5)//2 4 6 7 8 //1 3 5
            {
                rb.velocity = Vector2.zero;
                rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга

            }

            if (side == true && phase != 8 || side == true && phase != 9)//крутим врага в сторону когда он ударился в стенку
            {
                side = false;
            }
            else
            {
                side = true;
            }
        }
        if (collision.gameObject.tag == "EnemyJumpColl")//если мы наткнулись на коллайдер подпрыга врага
        {

            rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга

        }

    }

    private void OnTriggerExit2D(Collider2D collision)//обработка столкновений
    {
        if (collision.gameObject.tag == "EnemyReturnColl")//если мы наткнулись на коллайдер розворота врага
        {
            //WallTouch = false;
        }
    }

}
