﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelJungle_Slug : MonoBehaviour
{




    Animator anim;
    Rigidbody2D rb;
    public GameObject Player1;
    public GameObject Player2;
    public float jumpTime;
    public float startTime;
    public float moveSpeed = 1f;
    private float dividerMove = 15f;
    private bool isJump = false;
    private string animState = "Idle";
    // Use this for initialization
    public int HP = 2;                  // How many times the enemy can be hit before it dies.
    private bool run = false;
    private bool runStop = false;
    private bool runEnd = false;
    public bool side = true; //с какой стороны игрок, в какую бежать врагу при создании (true = право false - лево)
    public float HightEnemyJump = 5550f;
    public bool start = false;
    public bool live = false;
    public bool startRun = false;


    void Start()
    {


        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player2.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока

    }

    IEnumerator Jump()
    {

        yield return new WaitForSeconds(jumpTime);

        isJump = false;
        animState = "Idle";
        rb.velocity = Vector2.zero;
    }

    private void CheckEnemyHP()
    {
        if (GetComponent<EnemyHealth>().HP_enemy <= 0)//если у врага нету жизней убиваем его
        {
            Destroy(gameObject);
        }
    }


    IEnumerator StartTimer()
    {

        yield return new WaitForSeconds(startTime);
        //Debug.Log("!!!!!!!!!!!!");
        rb.simulated = true;
        StartCoroutine("StartRun");

        
    }

    IEnumerator StartRun()
    {

        yield return new WaitForSeconds(startTime);
        startRun = true;

        
    }


    // Update is called once per frame
    void Update()
    {

        CheckEnemyHP();

        if (start == true && live == false)
        {
            StartCoroutine("StartTimer");
            live = true;
        }

        if (isJump == false && 0 > 10)
        {
            //rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга

            if (animState != "JumpUp")
            {
                anim.SetTrigger("JumpUp");
                animState = "JumpUp";
            }


            StartCoroutine("Jump");
            isJump = true;
        }

    }

    public void EventRunStop()
    {
        runStop = true;
    }

    public void EventRunEnd()
    {
        runEnd = true;
        run = false;
        runStop = false;
    }


    private void FixedUpdate()
    {

        //Debug.Log(runStop);

        if (run == false && startRun == true && isJump == false)//Если закончил ходить

        {

            if (Random.Range(0, 10) > 7 && false)//Если случайность то прыгаем

            {

                if (animState != "JumpUp")
                {
                    anim.SetTrigger("JumpUp");
                    animState = "JumpUp";
                }


                if (side == true)
                {
                    //rb.AddForce(transform.right * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
                }
                else
                {
                    //rb.AddForce(transform.right * 2f * -4, ForceMode2D.Impulse);//добавляем вектор подпрыга
                }


                StartCoroutine("Jump");
                isJump = true;

            }


            else
            {
                run = true;
                runEnd = false;

                if (animState != "Run")
                {
                    anim.SetTrigger("Run");
                    animState = "Run";
                }
            }

            if (runStop == true)
            {
                rb.velocity = Vector2.zero;
            }

            if (run == true && runStop == false && runEnd == false)
            {
                //Debug.Log("!!!!!!!!!!!!!!!");

                if (side == true)
                {

                    transform.localRotation = Quaternion.Euler(0, 180, 0);
                    rb.velocity = new Vector2(transform.localScale.x * moveSpeed / dividerMove, GetComponent<Rigidbody2D>().velocity.y);
                    if (dividerMove <= 1f)
                    {
                        dividerMove = 1f;
                    }
                    else
                    {
                        dividerMove = dividerMove / 1.06f;
                    }

                }
                else
                {

                    transform.localRotation = Quaternion.Euler(0, 0, 0);
                    rb.velocity = new Vector2(transform.localScale.x * (-moveSpeed / dividerMove), GetComponent<Rigidbody2D>().velocity.y);
                    if (dividerMove <= 1f)
                    {
                        dividerMove = 1f;
                    }
                    else
                    {
                        dividerMove = dividerMove / 1.06f;
                    }
                }
            }

        }



    }


    private void OnTriggerEnter2D(Collider2D collision)//обработка столкновений
    {
        if (collision.gameObject.tag == "Player")//если мы наткнулись на игрока
        {
            collision.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, false);//приносим ему урон
        }
        if (collision.gameObject.tag == "EnemyReturnColl")//если мы наткнулись на коллайдер розворота врага
        {

            //rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
            if (side == true)//крутим врага в сторону когда он ударился в стенку
            {
                side = false;
            }
            else
            {
                side = true;
            }
        }
        if (collision.gameObject.tag == "EnemyJumpColl")//если мы наткнулись на коллайдер подпрыга врага
        {
            rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
        }

    }

    void OnCollisionStay2D(Collision2D other)
    {
        if (other.transform.tag == "Floor")
        {
            gameObject.tag = "Enemy";
            //this.GetComponent<CircleCollider2D>().enabled = true;
        }

    }
            
}
