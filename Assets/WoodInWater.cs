﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodInWater : MonoBehaviour
{

    public float MassMin = 2f;
    public float MassMax = 10f;
    private bool PlayerOn = false;
    Rigidbody2D rb;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        if (PlayerOn == true)
        {
            if (rb.mass < MassMax)
            {
                rb.mass = rb.mass * 1.2f;
            }
        }
        if (PlayerOn == false)
        {
            if (rb.mass > MassMin)
            {
                rb.mass = rb.mass * 0.9f;
            }
        }
    }

        void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Player")
        {
            PlayerOn = true;
        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.transform.tag == "Player")
        {
            PlayerOn = false;
        }
    }
}
