﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezieMove : MonoBehaviour {

    public int Xofset = 0;
    public int Yofset = 0;
    public float t = 0.5f; //lets get halfway down the curve
    public float speed = 0.01f; //lets get halfway down the curve
    private int Point = 0;
    private int Do = 0;//0-fly,1-fly to upp,2-stay in upp

    //private Vector2 temp;

    public GameObject Upp;

    public List<Vector2> StartPoints = new List<Vector2>();
    public List<float> Rotation = new List<float>();
    public List<int> Code = new List<int>();

    public List<Vector4> Temp = new List<Vector4>();


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        //Debug.Log(Point);

        if (Do == 0)
        {
            var ab = Vector2.Lerp(StartPoints[Point], StartPoints[Point + 1], t);
            var bc = Vector2.Lerp(StartPoints[Point + 1], StartPoints[Point + 2], t);
            transform.localPosition = Vector2.Lerp(ab, bc, t);


            transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(Rotation[Point], Rotation[Point + 2], t));

            if (t < 1f)
            {
                t = t + speed;
            }
            else
            {
                Point = Point + 2;
                t = t - 1;
                if (Point >= StartPoints.Count - 1)
                {
                    Point = 0;
                }
            }
        }
        if (Do == 1)
        {

            //temp = new Vector2(Upp.transform.position.x, Upp.transform.position.y);
            var ab = Vector2.Lerp(StartPoints[Point], StartPoints[Point + 1], t);
            var bc = Vector2.Lerp(StartPoints[Point + 1], new Vector2(Upp.transform.position.x + Xofset, Upp.transform.position.y), t);
            //var bc = Vector2.Lerp(StartPoints[Point + 1], Upp.transform.position, t);
            transform.localPosition = Vector2.Lerp(ab, bc, t);


            transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(Rotation[Point], Rotation[Point + 2], t));

            if (t < 1f)
            {
                t = t + speed;
            }
            else
            {
                Do = 2;
            }
        }
        if (Do == 2)
        {
            transform.localPosition = new Vector2(Upp.transform.position.x + Xofset, Upp.transform.position.y);
        }

        if (Code[Point + 2] == 1)
        {
            Do = 1;
        }


    }
}
