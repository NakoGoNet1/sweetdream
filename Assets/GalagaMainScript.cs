﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GalagaMainScript : MonoBehaviour {


    public GameObject Enemy;
    public GameObject ChangePos;
    private GameObject bulletInstance;
    public float TimeChangePosition;
    private bool isTimer = false;
    private int Next = 0;

    public List<Vector4> StartPoints = new List<Vector4>();

    public int[,] myArray = new int[10, 10];

    // Use this for initialization
    void Start () {

        //Debug.Log(StartPoints[1].x);
        //Debug.Log(StartPoints[1].y);
        //Debug.Log(StartPoints[1].z);
        //Debug.Log(StartPoints[1].w);

        myArray[0, 0] = 999;
        Debug.Log(myArray[0, 0]);
    }

    IEnumerator Timer()
    {

        yield return new WaitForSeconds(TimeChangePosition); isTimer = false; //Debug.Log("!!!!!!!!!!!!!");

        bulletInstance = Instantiate(Enemy, new Vector2(0, 6), Quaternion.identity);
        bulletInstance.gameObject.GetComponent<BezieMove>().Xofset = Next;
        Next++;

        bulletInstance.gameObject.GetComponent<BezieMove>().Temp = StartPoints;

        //bulletInstance.gameObject.GetComponent<BezieMove>().Upp = ChangePos;
        bulletInstance.gameObject.GetComponent<BezieMove>().Upp = ChangePos;
  
    }


    // Update is called once per frame
    void Update () {



        if (isTimer == false && Next < 15)
        {
            isTimer = true;
            StartCoroutine("Timer");
        }

		
	}
}
