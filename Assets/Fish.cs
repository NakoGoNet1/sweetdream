﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour {

    public GameObject Player1;
    public GameObject Player2;
    public GameObject PlayerCenter;
    public GameObject AggrePlayer;
    public GameObject LeftRange;
    public GameObject RightRange;
    public float Range = 2f;
    public float Speed = 10f;
    public float TimeChangePosition = 1f;
    private float transformx = 0;
    private float transformy = 0;
    private float position_y = 0;
    public int pointx = 1;//точка где висит враг (0-лево, 1-право, 2 - на игрока летит)
    private int point = 1;
    private bool changePos = false;
    public bool Damage;
    public float HightEnemyJump = 5550f;
    private bool side = true;// true - right false - left
    private bool OldSide = true;
    private bool Swim = false;
    /// </summary>
    Animator anim;
    // Use this for initialization


    void Start () {
        position_y = transform.position.y;
        anim = GetComponent<Animator>();
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player2.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        SelectAggrePlayer();
    }

    private void SelectAggrePlayer()
    {
        //select aggrePlayer
        if (Player1.GetComponent<Mario1Controller2DScript>().Dead == false && Player2.GetComponent<Mario1Controller2DScript>().Dead == false)
        {
            //Debug.Log(Random.Range(-2f, 2f));
            if (Random.Range(-2f, 2f) > 0)
            {
                AggrePlayer = Player2;
            }
            else
            {
                AggrePlayer = Player1;
            }
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            AggrePlayer = Player2;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            AggrePlayer = Player1;
        }
        if (Player1.GetComponent<Mario1Controller2DScript>().Dead == true && Player2.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            point = 3;
        }
    }

    IEnumerator waitToChangePos()
    {

        yield return new WaitForSeconds(TimeChangePosition); changePos = false; //Debug.Log("!!!!!!!!!!!!!");
        switch (point)
        {
            case 1:
                point = 2;
                break;
            case 2:
                point = 1;
                break;
        }
    
    }

    private void FixedUpdate()
    {
        //Debug.Log("!!!!!!!!!!!!!!" + Damage);

        OldSide = side;
        if (transform.position.x < AggrePlayer.transform.position.x) // поворот врага в зависимости с какой стороны игрок
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
            side = false;
        }
        else
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            side = true;
        }

        if (OldSide != side)
        {
            anim.SetTrigger("Turn"); Swim = false;
        }

        if (changePos == false)
        {
            StartCoroutine("waitToChangePos"); changePos = true;
        }


        if (point == 1 && Damage == false)
        {

                transformx = (transform.position.x - AggrePlayer.transform.position.x) + Range;
            if (transform.position.x - (transformx / Speed) > LeftRange.transform.position.x && transform.position.x - (transformx / Speed) < RightRange.transform.position.x)
            {
                transform.position = new Vector2(transform.position.x - (transformx / Speed), position_y);
            }

            


        }
        if (point == 2 && Damage == false)
        {
                transformx = (transform.position.x - AggrePlayer.transform.position.x) - Range;
            if (transform.position.x - (transformx / Speed) < RightRange.transform.position.x && transform.position.x - (transformx / Speed) > LeftRange.transform.position.x)
            {
                transform.position = new Vector2(transform.position.x - (transformx / Speed), position_y);
            }
           

        }

        if (Damage == true)
        {
            anim.SetTrigger("Attack"); Swim = false;
            transformx = transform.position.x - AggrePlayer.transform.position.x;
            transformy = transform.position.y - AggrePlayer.transform.position.y;
            transform.position = new Vector2(transform.position.x - (transformx / Speed), transform.position.y - (transformy / Speed));
            
        }
        else
        {
            if (Swim == false)
            {
                anim.SetTrigger("Swim"); //Debug.Log("Swim");
                Swim = true;
            }
        }

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, true);
        }

    }



    // Update is called once per frame
    void Update () {
		
	}
}
