﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquirrelRunCircle : MonoBehaviour {


    public GameObject Player;
    public bool live = false;
    public bool runToPlayer = false;
    private bool aggred = false;
    public int Power;
    public float deadTime;
    public float whistlingTime;
    public float aggredTime;
    Animator anim;
    private string animState = "Run";
    private float transformx;
    private float transformy;
    public float runToPlayerSpeed;
    public bool bossHurt = false;

    public void StartAggre()
    {
        live = true;
        StartCoroutine("waitToWhistling");
        
    }

    IEnumerator waitToWhistling()
    {

        yield return new WaitForSeconds(whistlingTime);

        StartCoroutine("waitToDead");

        AudioSource sound = gameObject.GetComponent<AudioSource>();
        sound.Play();

    }
    IEnumerator waitToDead()
    {

        yield return new WaitForSeconds(deadTime);

        live = false;

        transform.position = new Vector2(-10f, -10f);
        
    }


    IEnumerator waitToStopAggred()
    {

        yield return new WaitForSeconds(aggredTime);

        aggred = false;
        runToPlayer = true;

    }


    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        if (live == true && aggred == false && runToPlayer == false)
        {
            transform.position = new Vector2(Player.transform.position.x, Player.transform.position.y);

            if (animState != "Run")
            {
                anim.SetTrigger("Run");
                animState = "Run";
            }
        }

        if (runToPlayer == true && live == true)
        {
            transformx = transform.position.x - Player.transform.position.x;
            transformy = transform.position.y - Player.transform.position.y;

            transform.position = new Vector2(transform.position.x - (transformx / runToPlayerSpeed), transform.position.y - (transformy / runToPlayerSpeed));

            if (Mathf.Abs(transformx) < 2f && Mathf.Abs(transformy) < 2f)
            {
                runToPlayer = false;
            }
            //if (animState != "RunOnPlayer")
            //{
            //    anim.SetTrigger("RunOnPlayer");
            //    animState = "RunOnPlayer";
            //}
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        // If it hits an enemy...
        if (col.tag == "Enemy")
        {
            // ... find the Enemy script and call the Hurt function.
            if (col.gameObject.GetComponent<LevelJungleBoss>() != null)
            {
                if (bossHurt == false)
                {
                    col.gameObject.GetComponent<EnemyHealth>().Hurt(Power);
                    bossHurt = true;
                }
            }
            else
            {
                col.gameObject.GetComponent<EnemyHealth>().Hurt(Power);
            }
            
            aggred = true;
            transform.position = new Vector2(col.gameObject.transform.position.x, col.gameObject.transform.position.y);
            StartCoroutine("waitToStopAggred");

            if (animState != "Aggre")
            {
                anim.SetTrigger("Aggre");
                animState = "Aggre";
            }

                

        }
    }
}
