﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelJungleGodRay : MonoBehaviour {


    public GameObject Camera;
    public float x;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        transform.localRotation = Quaternion.Euler(0, 0, Camera.transform.position.x * x);

    }
}
