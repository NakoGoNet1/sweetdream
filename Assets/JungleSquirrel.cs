﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JungleSquirrel : MonoBehaviour {



    public float moveSpeed = 1f;
    public float jumpSpeed = 4f;
    public float JumpTime = 0.2f;
    public float jumpLeftSpeed = 4f;
    public GameObject Player1;
    public GameObject Player2;
    public GameObject PlayerCenter;
    public GameObject LeftBorder;
    public GameObject RightBorder;
    public GameObject LeftRange;
    public GameObject RightRange;
    public int HP = 2;					// How many times the enemy can be hit before it dies.

    Rigidbody2D rb;
    private bool visible = false;
    private bool dead = false;
    public bool take = false;
    private bool run = false;
    private bool canrun = false;
    private bool side = false;//true - right false - left
    Animator anim;
    private string animState = "Idle";

    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, new Vector3(LeftBorder.transform.position.x, LeftBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(RightBorder.transform.position.x, RightBorder.transform.position.y, 0.0f));
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, new Vector3(LeftRange.transform.position.x, LeftRange.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(RightRange.transform.position.x, RightRange.transform.position.y, 0.0f));

    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
    // Use this for initialization
    void Start () {

        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player2.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        gameObject.tag = "Untagged";//изменим тег врага что бы изначально игрок не мог в него попасть
    }
	
	// Update is called once per frame
	void Update () {

        if (GetComponent<EnemyHealth>().HP_enemy <= 0 && dead == false)//если у врага нету жизней убиваем его
        {
            if (animState != "Death")
            {
                anim.SetTrigger("Death");
                animState = "Death";
            }

            dead = true;
            take = true;
            rb.velocity = Vector2.zero;
            gameObject.tag = "Untagged";
        }

    }

    private void Visible()
    {
        //Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (PlayerCenter.transform.position.x > LeftBorder.transform.position.x && PlayerCenter.transform.position.x < RightBorder.transform.position.x)
        {
            visible = true;
            rb.AddForce(transform.up * 2f * jumpSpeed, ForceMode2D.Impulse);//добавляем вектор подпрыга
            StartCoroutine("waitToRun");
        }
    }

    private void checkIfOutOfScreen()
    {
        if (transform.position.x > LeftRange.transform.position.x && transform.position.x < RightRange.transform.position.x)
        {
            dead = true;
            //Destroy(gameObject);
        }
    }

    IEnumerator waitToRun()
    {
        yield return new WaitForSeconds(JumpTime);
        canrun = true;
        if (animState != "Run")
        {
            anim.SetTrigger("Run");
            animState = "Run";
        }
    }


    void FixedUpdate()
    {
        if (visible == false && dead == false)
        {
            Visible();
        }
        if (visible == true && dead == false && run == true)
        {
            if (side == false)
            {
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                rb.velocity = new Vector2(transform.localScale.x * (-moveSpeed), GetComponent<Rigidbody2D>().velocity.y);//двигаем врага влево
            }
            else
            {
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                rb.velocity = new Vector2(transform.localScale.x * (moveSpeed), GetComponent<Rigidbody2D>().velocity.y);//двигаем врага вправо
            }
         
        }
        if (dead == false)
        {
            checkIfOutOfScreen();
        }
        
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Floor" && dead == false && canrun == true || other.gameObject.tag == "Platform" && dead == false && canrun == true)//
        {
            run = true;
            gameObject.tag = "Enemy";//изменим тег врага после того как он подпрыгнул и начал бежать что-бы игрок мог убить его
            //Debug.Log("!!!!!!!!!!!!!!!!");
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)//обработка столкновений
    {

        if (collision.gameObject.tag == "EnemyReturnColl")//если мы наткнулись на коллайдер розворота врага
        {
            rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
            if (side == true)//крутим врага в сторону когда он ударился в стенку
            {
                side = false;
            }
            else
            {
                side = true;
            }
        }
        if (collision.gameObject.tag == "EnemyJumpColl")//если мы наткнулись на коллайдер подпрыга врага
        {
            rb.AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
        }

    }



}
