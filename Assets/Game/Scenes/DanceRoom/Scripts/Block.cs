﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
	public GameObject blockDefault;
	public GameObject blockClicked;
	
	private bool _modified = false;
	

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("DanceRoom_screen"))
		{
			if (_modified)
			{
				Destroy(this.gameObject);	
			}
			else
			{
				blockDefault.SetActive(false);
				_modified = true;
			}
		}
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("DanceRoom_screen"))
		{
			blockDefault.SetActive(true);
		}
	}
}
