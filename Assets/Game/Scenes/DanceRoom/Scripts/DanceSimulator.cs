﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DanceSimulator : MonoBehaviour
{
    public GameObject block;
    
    List<GameObject> _blockOnScreenList = new List<GameObject>();
    private float _timerBlock = 0.0f;
    private float _timerValue = 2.0f;

    private class Block
    {
        
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (_timerBlock <= 0.0f)
        {
            GameObject instance;

            var vector3 = new Vector3(this.transform.position.x - 6, this.transform.position.y + 6, this.transform.position.z - 1);
            instance = Instantiate(block,vector3,
                transform.rotation) as GameObject;
            instance.transform.parent = this.transform;
            _blockOnScreenList.Add(instance);
            _timerBlock = _timerValue;
        }
        else
        {
            _timerBlock -= Time.deltaTime;
        }
    }
}