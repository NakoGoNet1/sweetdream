﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;//для работы со сценами

public class PlayerDamage : MonoBehaviour {

    int Life = 5;
    bool isdeath = false;
    public GameObject MainImage;
    // Use this for initialization
    void Start () {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)//если игрок наткнулся на врага по триггеру
    {
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "EnemySpider" && isdeath == false)//проверка столкновения
        {
            if (collision.gameObject.transform.position.x > transform.position.x)
            { //Debug.Log("left");
            isdeath = true;
            Invoke("PauseDeath", 3);
            Life--;
            MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 0.30196078f, 0.30196078f); }
            if (collision.gameObject.transform.position.x < transform.position.x)
            { //Debug.Log("right");
            isdeath = true;
            Invoke("PauseDeath", 3);
            Life--; MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 0.30196078f, 0.30196078f); }
            //Invoke("PauseDeath", 3); //Вызов перезагрузки сцены
            //Life--;
            //isdeath = true;

        }
    }
    void OnCollisionEnter2D(Collision2D collision)//если игрок наткнулся на врага по триггеру
    {
     if (collision.gameObject.tag == "EnemyBottom" && isdeath == false)
        {
            
            isdeath = true;
            Invoke("PauseDeath", 3);
            Life--;
            MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 0.30196078f, 0.30196078f);
        }
    }


    public void Damage()
    {
        isdeath = true; Invoke("PauseDeath", 3); Life--; //MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 0.30196078f, 0.30196078f);
    }

    private void OnGUI()
    {
        
            GUI.Box(new Rect(0, 0, 100, 30), "Life = " + Life);
        
    }

    void PauseDeath()
    {
        //MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f);//делаем героя назад белым цветом
        isdeath = false;
    }

    //void ReloadLevel()
    //{
    //    Application.LoadLevel(Application.loadedLevel);//метод перезагрузки уровня

    // }

    // Update is called once per frame
    void Update () {

        //Debug.Log(angle);


    }
    //public float GetCollisionAngle(Transform hitobjectTransform, CircleCollider2D collider, Vector2 contactPoint)
    //{
    //    Vector2 collidertWorldPosition = new Vector2(hitobjectTransform.position.x, hitobjectTransform.position.y);
    //    Vector3 pointB = contactPoint - collidertWorldPosition;
//
    //    float theta = Mathf.Atan2(pointB.x, pointB.y);
    //    angle = (360 - ((theta * 180) / Mathf.PI)) % 360;
    //    return angle;
    //}
}
