﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelJungleBoss_rocket : MonoBehaviour
{

    public GameObject Player1;
    public GameObject Player2;

    public float t = 0.5f; //lets get halfway down the curve
    public float speed = 0.01f; //lets get halfway down the curve
    public float speedBezie = 0.01f; //lets get halfway down the curve
    //private int Point = 0;

    public int HP = 2;                  // How many times the enemy can be hit before it dies.
    //public Vector2 StartPoints0;
    //public Vector2 StartPoints1;
    //public Vector2 StartPoints2;

    public GameObject StartPoints0;
    public GameObject StartPoints1;
    public GameObject StartPoints2;
    private Vector2 StatrPoint2Rand;
    public float rotatingSpeed = 200;

    public GameObject target;
    public GameObject boss;

    public float HightEnemyJump = 5550f;

    Rigidbody2D rb;

    public int phase = 0;

    private Vector3 v_diff;
    private float atan2;

    // Use this for initialization
    void Start()
    {
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player2.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока

        rb = GetComponent<Rigidbody2D>();
        rb.simulated = false;

        StatrPoint2Rand.x = StartPoints2.transform.position.x + Random.Range(-2f, 2f);
        StatrPoint2Rand.y = StartPoints2.transform.position.y;
    }



    private void CheckOutOfScreen()
    {
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPosition.x < 0 || screenPosition.x > Screen.width || screenPosition.y < 0 || screenPosition.y > Screen.height)
        {
            Destroy(gameObject);
        }
    }


    private void CheckEnemyHP()
    {
        if (GetComponent<EnemyHealth>().HP_enemy <= 0)//если у врага нету жизней убиваем его
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {

        CheckEnemyHP();

        if (phase == 0 && boss.gameObject != null)
        {
            var ab = Vector2.Lerp(StartPoints0.transform.position, StartPoints1.transform.position, t);
            var bc = Vector2.Lerp(StartPoints1.transform.position, StatrPoint2Rand, t);
            transform.transform.position = Vector2.Lerp(ab, bc, t);






            //v_diff = (Player1.transform.position - transform.position); что бы крутились в направлении игрока
            //atan2 = Mathf.Atan2(v_diff.y, v_diff.x);
           


            if (boss.GetComponent<LevelJungleBoss>().side == true)
            {
                transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(45f, -90f, t));
                //transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(45f, atan2 * Mathf.Rad2Deg, t));//что бы крутились в направлении игрока
            }
            else
            {
                transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(125f, 270f, t));
               // transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(125f, atan2 * Mathf.Rad2Deg, t));//что бы крутились в направлении игрока
            }


            if (t < 1f)
            {
                t = t + speedBezie;
            }
            else
            {
                phase = 1;
                rb.simulated = true;
            }
        }



    }

    private void FixedUpdate()
    {
        if (phase == 1)
        {
            CheckOutOfScreen();

            Vector2 point2Target = (Vector2)transform.position - (Vector2)target.transform.position;

            point2Target.Normalize();

            float value = Vector3.Cross(point2Target, transform.right).z;

            /*
            if (value > 0) {

                    rb.angularVelocity = rotatingSpeed;
            } else if (value < 0)
                    rb.angularVelocity = -rotatingSpeed;
            else
                    rotatingSpeed = 0;
    */

            rb.angularVelocity = rotatingSpeed * value;


            rb.velocity = transform.right * speed;
        }


    }


    private void OnTriggerEnter2D(Collider2D collision)//обработка столкновений
    {
        if (collision.gameObject.tag == "Player")//если мы наткнулись на игрока
        {
            collision.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, false);//приносим ему урон
        }





    }
}
