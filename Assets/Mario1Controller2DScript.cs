﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;//для работы со сценами

public class Mario1Controller2DScript : MonoBehaviour
{

    //Movement related variables
    private float t = 0;
    public bool GodMode = true;
    public GameObject Player2; //второй плеер для места создания первого
    public GameObject Point_01; //точка появление игрока на кнопку 1
    public GameObject Point_02; //точка появление игрока на кнопку 2
    public GameObject Point_03; //точка появление игрока на кнопку 3
    public int PlayerCode;
    public int PlayerRocketPower;
    public bool Dead = false;
    public GameObject PlayerName;
    public GameObject PlayerPressAnyKey;
    public GameObject PlayerHearth1;
    public GameObject PlayerHearth2;
    public GameObject PlayerHearth3;
    public GameObject PlayerHearth4;
    public GameObject PlayerHearth5;
    public GameObject PlayerHearth6;
    public GameObject PlayerSquirrel1;
    public GameObject PlayerSquirrel2;
    public GameObject PlayerSquirrel3;
    public GameObject PlayerSquirrel4;
    public GameObject PlayerSquirrel5;
    public GameObject PlayerSquirrel6;
    public string Jump;
    public string Horizontal;
    public string Vertical;
    public string Fire;
    public string Fire1;
    public float moveSpeed;  //Our general move speed. This is effected by our
                             //InputManager > Horizontal button's Gravity and Sensitivity
                             //Changing the Gravity/Sensitivty will in turn result in more loose or tighter control


    float Speed = 0;//Don't touch thi
    public float MaxSpeed = 7000;//This is the maximum speed that the object will achieve
    public float Acceleration = 1000;//How fast will object reach a maximum speed
    public float Deceleration = 1000;//How fast will object reach a speed of 0

    public float sprintMultiplier;   //How fast to multiply our speed by when sprinting
    public float sprintDelay;        //How long until our sprint kicks in
    private float sprintTimer;       //Used in calculating the sprint delay
    private bool jumpedDuringSprint; //Used to see if we jumped during our sprint

    //Jump related variables
    public float initialJumpForce;       //How much force to give to our initial jump
    public float extraJumpForce;         //How much extra force to give to our jump when the button is held down
    public float maxExtraJumpTime;       //Maximum amount of time the jump button can be held down for
    public float delayToExtraJumpForce;  //Delay in how long before the extra force is added
    private float jumpTimer;             //Used in calculating the extra jump delay
    private bool playerJumped;           //Tell us if the player has jumped
    private bool playerJumping;          //Tell us if the player is holding down the jump button
    public Transform groundChecker;      //Gameobject required, placed where you wish "ground" to be detected from
    public bool isGrounded;             //Check to see if we are grounded
    private bool onPlatform = false;
    private bool jumpDown = false;
    private float Times;
    private GameObject jk;
    Animator anim;
    Rigidbody2D rb;
    public GameObject MainImage;
    public GameObject MainCamera;
    public Rigidbody2D rocket;
    public Rigidbody2D second_gun;
    public GameObject hero_shadow;
    private bool isPlayerLeftOrRight = true;//rotation of Player (true=right false=left)
    private bool isPlayerOnWall = false;
    public float fireRate = 10.5f;
    private float nextFire = 0.0f;
    public float speed = 20f;
    public float RandomFire = 0.2f;
    public int Life = 5;
    public int Belka = 0;
    private bool isdeath = false;
    private bool sit = false;//переменная для белки, тру когда мы присели
    private bool cantMove = false;//переменная для для демеджа игрока что бы не могли двигаться времено при уроне

    private bool isKeyDownJump;
    private bool isKeyDownFire;
    private float inputAxisX;
    private float inputAxisY;
    private bool isKeyDownLeft;
    private bool isKeyDownRight;
    private bool isKeyDownDow;
    private bool isKeyDownUp;



    public float spriteBlinkingTimer = 0.0f;
    public float spriteBlinkingMiniDuration = 0.1f;
    public float spriteBlinkingTotalTimer = 0.0f;
    private bool flash = true;
    private string animState = "1";
    private int AnimJumpFrame;
    private bool Hurt = false;

    void Awake()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        if (PlayerCode == 1)
        {
            Dead = true;
            DeadPlayer();
            
        }
    }

    private void DeadPlayer()
    {
        MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);  //make changes
        rb.simulated = false;
        rb.velocity = Vector2.zero;
        transform.localPosition = new Vector2(0, 10f);
    }

    private void CreatePlayer()

    {
        if (Life > 0)
        {
            MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

            rb.simulated = true;
            rb.velocity = new Vector2(0, 8550 / 400);
            transform.localPosition = new Vector2(Player2.transform.position.x, Player2.transform.position.y + 2f);
            Dead = false;
            Life--;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Life > 1)
        {

            MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

            rb.simulated = true;
            rb.velocity = new Vector2(0, 8550 / 400);
            transform.localPosition = new Vector2(Player2.transform.position.x, Player2.transform.position.y + 2f);
            Dead = false;
            Player2.GetComponent<Mario1Controller2DScript>().Life--;
            Life = 1;
        }

    }
    private void SetRocketPower()
    {
        if (Player2.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            PlayerRocketPower = 10;
        }
        else
        {
            PlayerRocketPower = 5;
        }
    }

    private void PlayerHearth()
    {
        if (Dead == true)
        {
            PlayerName.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth1.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth2.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth3.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth4.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth5.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth6.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel1.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel2.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel3.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel4.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel5.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel6.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (Dead == false && Life == 1)
        {
            PlayerName.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth1.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth2.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth3.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth4.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth5.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth6.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (Dead == false && Life == 2)
        {
            PlayerName.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth1.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth2.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth3.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth4.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth5.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth6.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (Dead == false && Life == 3)
        {
            PlayerName.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth1.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth2.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth3.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth4.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth5.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth6.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (Dead == false && Life == 4)
        {
            PlayerName.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth1.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth2.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth3.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth4.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth5.GetComponent<SpriteRenderer>().enabled = false;
            PlayerHearth6.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (Dead == false && Life == 5)
        {
            PlayerName.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth1.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth2.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth3.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth4.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth5.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth6.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (Dead == false && Life > 5)
        {
            PlayerName.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth1.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth2.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth3.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth4.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth5.GetComponent<SpriteRenderer>().enabled = true;
            PlayerHearth6.GetComponent<SpriteRenderer>().enabled = true;
        }
        if (Belka == 0)
        {
            PlayerSquirrel1.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel2.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel3.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel4.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel5.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel6.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (Belka == 1)
        {
            PlayerSquirrel1.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel2.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel3.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel4.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel5.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel6.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (Belka == 2)
        {
            PlayerSquirrel1.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel2.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel3.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel4.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel5.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel6.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (Belka == 3)
        {
            PlayerSquirrel1.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel2.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel3.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel4.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel5.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel6.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (Belka == 4)
        {
            PlayerSquirrel1.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel2.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel3.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel4.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel5.GetComponent<SpriteRenderer>().enabled = false;
            PlayerSquirrel6.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (Belka == 5)
        {
            PlayerSquirrel1.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel2.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel3.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel4.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel5.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel6.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (Belka > 5)
        {
            PlayerSquirrel1.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel2.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel3.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel4.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel5.GetComponent<SpriteRenderer>().enabled = true;
            PlayerSquirrel6.GetComponent<SpriteRenderer>().enabled = true;
        }

        if (Dead == true && Player2.GetComponent<Mario1Controller2DScript>().Life > 1)
        {
            PlayerPressAnyKey.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            PlayerPressAnyKey.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    public void HurtFalse()
    {
        Hurt = false;
    }

    public void Damage(float Hight, bool Floor)//Hight - высота прижка при попадании на врага, Floor - true - попали на яму, прыгать все время
    {





        if (isdeath == false && second_gun.GetComponent<SquirrelRunCircle>().live == false && GodMode != true)
        {

            Hurt = true;
            if (animState != "Hurt")
            {
                anim.SetTrigger("Hurt");
                animState = "Hurt";
            }

            cantMove = true; Invoke("PauseCantMove", 0.5f);
            rb.velocity = Vector2.zero;
            //rb.AddForce(new Vector2(0, Hight));
            if (isPlayerLeftOrRight == true)
            {
                rb.velocity = new Vector2(-2, Hight / 400);
            }
            else if (isPlayerLeftOrRight == false)
            {
                rb.velocity = new Vector2(2, Hight / 400);
            }

                isdeath = true; Invoke("PauseDeath", 3); Life--; //MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 0.30196078f, 0.30196078f);
            //Debug.Log("We shooted by enemy"+ Hight);

        }
        else
        {
            if (Floor == true)// если мы попали на яму
            {
                rb.velocity = new Vector2(0, Hight / 400);//прыгаем
            }
        }
    }
    void PauseDeath()//задержка для неуранимости игрока если он получил урон
    {
        isdeath = false;
        MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);//возвращаем назад прозрачность нашего игрока

        rb.WakeUp();//поднимаем со сна нашего игрока для срабатывания коллайдера урона
        //GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 100f));
    }

    void PauseCantMove()//задержка для отключения недвижимости игрока при уроне
    {


        cantMove = false;
    }

    //private void OnGUI()
    //{
    //    if (PlayerCode == 0)
    //    {
    //        GUI.Box(new Rect(0, 0, 100, 30), "Life = " + Life);
    //        GUI.Box(new Rect(150, 0, 100, 30), "Belka = " + Belka);

    //    }
    //    else if (PlayerCode == 1)
    //    {
    //        GUI.Box(new Rect(350, 0, 100, 30), "Life = " + Life);
    //        GUI.Box(new Rect(500, 0, 100, 30), "Belka = " + Belka);

    //    }
    //}
    private void StartBlinkingEffect()
    {
        spriteBlinkingTotalTimer += Time.deltaTime;
        spriteBlinkingTimer += Time.deltaTime;
        if (spriteBlinkingTimer >= spriteBlinkingMiniDuration)
        {
            spriteBlinkingTimer = 0.0f;
            if (flash == true)
            {
                flash = false;
                MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.2f);  //make changes
            }
            else
            {
                flash = true;
                MainImage.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);   //make changes
            }
        }
    }



    public void AnimFrameJump(int JumpFrame)
    {
        AnimJumpFrame = JumpFrame;
        //Debug.Log(AnimJumpFrame);
    }


    private void Set_animation()
    {

    }


    private void Set_shadow()
    {
        if (isGrounded == true)
        {
            hero_shadow.GetComponent<SpriteRenderer>().color += new Color(1, 1, 1, 1);
        }
        else if (isGrounded == false)
        {
            hero_shadow.GetComponent<SpriteRenderer>().color += new Color(1, 1, 1, 1);
        }
    }





    void Update()
    {
        //Casts a line between our ground checker gameobject and our player
        //If the floor is between us and the groundchecker, this makes "isGrounded" true
        //isGrounded = Physics2D.Linecast(transform.position, groundChecker.position, 1 << LayerMask.NameToLayer("Floor"));
        //Debug.Log(isGrounded);
        //If our player hit the jump key, then it's true that we jumped!


        


        SetRocketPower();
        Set_animation();
        Set_shadow();
        //PlayerHearth();

        if (Life <= 0)//если жизни равны 0 - перезагружаем уровень
        {
            //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            Dead = true;
            DeadPlayer();
        }

        if (Dead == false)
        {

            if (isdeath == true)//мигаем игроком если он получил урон
            {
                StartBlinkingEffect();
            }

            if (Input.GetButtonDown(Jump) && isGrounded)
            {
                playerJumped = true;   //Our player jumped!
                playerJumping = true;  //Our player is jumping!
                jumpTimer = Time.time; //Set the time at which we jumped
            }

            if (Input.GetKeyUp(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }

            if (Input.GetKeyUp(KeyCode.Alpha1))
            {
                transform.position = Point_01.transform.position;
            }
            if (Input.GetKeyUp(KeyCode.Alpha2))
            {
                transform.position = Point_02.transform.position;
            }
            if (Input.GetKeyUp(KeyCode.Alpha3))
            {
                transform.position = Point_03.transform.position;
            }

            if (Input.GetKeyUp(KeyCode.LeftControl))//ускорение для быстрого передвижения
            {
                Debug.Log("22222");
                //rb.velocity = new Vector2(400, rb.velocity.y);
                //rb.velocity = new Vector2(1 * Speed * Time.deltaTime, rb.velocity.y);
                //Speed = 2f;
                if (isPlayerLeftOrRight)
                {
                    t = 1.4f;
                }
                else
                {
                    t = 1.4f;
                }
  
                //rb.AddForce(transform.up * 2f * 120, ForceMode2D.Impulse);//при прыжке если нажать вниз добавляем ускорение для быстрого опускания
            }
            



            if (Input.GetButtonDown(Fire1) && Belka > 0 && second_gun.GetComponent<SquirrelRunCircle>().live == false || Input.GetKeyUp(KeyCode.E) && Belka > 0 && second_gun.GetComponent<SquirrelRunCircle>().live == false)
            {
                Belka--;
                second_gun.GetComponent<SquirrelRunCircle>().StartAggre();
            }

            if (Input.GetButtonDown(Jump) && Times == 5f)
            {
                playerJumped = true;   //Our player jumped!
                playerJumping = true;  //Our player is jumping!
                jumpTimer = Time.time; //Set the time at which we jumped
                                       //Debug.Log("22222");
            }

            //If our player lets go of the Jump button OR if our jump was held down to the maximum amount...
            if (Input.GetButtonUp(Jump) || Time.time - jumpTimer > maxExtraJumpTime)
            {
                playerJumping = false; //... then set PlayerJumping to false
                                       //
            }

            //If our player hit a horizontal key...
            if (Input.GetButtonDown(Horizontal))
            {
                sprintTimer = Time.time;  //.. reset the sprintTimer variable
                jumpedDuringSprint = false;  //... change Jumped During Sprint to false, as we lost momentum

            }


            isKeyDownJump = Input.GetButton(Jump);
            isKeyDownFire = Input.GetButton(Fire);
            inputAxisX = Input.GetAxisRaw(Horizontal);
            inputAxisY = Input.GetAxisRaw(Vertical);
            isKeyDownLeft = inputAxisX < -0.5f;
            isKeyDownRight = inputAxisX > 0.5f;
            isKeyDownDow = inputAxisY < -0.5f;
            isKeyDownUp = inputAxisY > 0.5f;


            if (isKeyDownDow == true && isGrounded == true && isKeyDownLeft == false && isKeyDownRight == false)//приседание
            {
                //Debug.Log("Down");

                if (animState != "4" && Hurt == false)
                {
                    anim.SetTrigger("4");
                    animState = "4";
                }

            }


            if (isKeyDownDow == true && isGrounded == true && isKeyDownLeft == false && isKeyDownRight == false)//Для белки код!!!!
            {
                if (animState != "4" && Hurt == false)
                {
                    anim.SetTrigger("4");
                    animState = "4";
                }
                sit = true;//для белки делаем тру что бы понятно что мы присели
            }
            else
            {
                sit = false;
            }

            if (isKeyDownRight == true)//rotation
            {
                MainImage.transform.localRotation = Quaternion.Euler(0, 0, 0); isPlayerLeftOrRight = true;// Debug.Log("isGrounded");
            }

            if (isKeyDownLeft == true)//rotation
            {
                MainImage.transform.localRotation = Quaternion.Euler(0, 180, 0); isPlayerLeftOrRight = false;// Debug.Log("isGrounded");
            }

            if (isKeyDownRight == true && isGrounded == true)
            {
                
                if (animState != "2" && animState == "1" && Hurt == false)
                {
                    anim.SetTrigger("6");//было 2 для плавного старта при переходе с айдл на бег
                    animState = "2";
                }
                else if (animState != "2" && Hurt == false)
                {
                    anim.SetTrigger("6");
                    animState = "2";
                }
            }

            if (isKeyDownLeft == true && isGrounded == true)
            {
                MainImage.transform.localRotation = Quaternion.Euler(0, 180, 0); isPlayerLeftOrRight = false;// Debug.Log("isGrounded");
                if (animState != "2" && animState == "1" && Hurt == false)
                {
                    anim.SetTrigger("6");//было 2 для плавного старта при переходе с айдл на бег
                    animState = "2";
                }
                else if (animState != "2" && Hurt == false)
                {
                    anim.SetTrigger("6");
                    animState = "2";
                }

            }

            if (isKeyDownFire == true)
            {
                
                anim.SetFloat("Blend", 1f);
            }
            else
            {
                anim.SetFloat("Blend", 0f);
            }

            if (isKeyDownLeft == false && isKeyDownRight == false && isKeyDownDow == false && isGrounded == true)
            {
                
                if (animState != "1" && animState != "2" && Hurt == false)
                {
                    anim.SetTrigger("1");
                    animState = "1";
                }
                else if (animState != "1" && animState == "2" && Hurt == false)
                {
                    anim.SetTrigger("1");//было 7 для плавной остановки при беге на айдл
                    animState = "1";
                }
            }
            if (isGrounded == false)// && isKeyDownJump == true //прыжок
            {
                
                if (animState != "3" && Hurt == false)//если уже мы не включили анимацию прыжка
                {
                    //if (animState == "1")//если мы прыгаем с idle
                    //{
                        anim.SetTrigger("3");
                        animState = "3";
                    //}
                    //else if (AnimJumpFrame == 1 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_01");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 2 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_02");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 3 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_03");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 4 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_04");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 5 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_05");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 6 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_06");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 7 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_07");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 8 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_08");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 9 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_09");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 10 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_10");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 11 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_11");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 12 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_12");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 13 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_13");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 14 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_14");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 15 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_15");
                    //    animState = "3";
                    //}
                    //else if (AnimJumpFrame == 16 && animState != "3")
                    //{
                    //    anim.SetTrigger("Jump_16");
                    //    animState = "3";
                    //}


                }
            }


            //Debug.Log(animState);

            ///////FIRE START
            if (isKeyDownFire == true && Time.time > nextFire)
            {
                if (isKeyDownUp == false && isKeyDownDow == false && isPlayerLeftOrRight == true)//стрельба вправо
                {
                    nextFire = Time.time + fireRate; Rigidbody2D bulletInstance = Instantiate(rocket, new Vector2(transform.position.x + 1.3f, transform.position.y + (-0.2f + Random.Range(-RandomFire, RandomFire))), Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
                    bulletInstance.velocity = new Vector2(speed, 0); bulletInstance.GetComponent<Rocket>().RocketPower = PlayerRocketPower;
                }
                if (isKeyDownUp == false && isKeyDownDow == false && isPlayerLeftOrRight == false)//стрельба влево
                {
                    nextFire = Time.time + fireRate; Rigidbody2D bulletInstance = Instantiate(rocket, new Vector2(transform.position.x - 1.3f, transform.position.y + (-0.2f + Random.Range(-RandomFire, RandomFire))), Quaternion.Euler(new Vector3(0, 0, 180f))) as Rigidbody2D;
                    bulletInstance.velocity = new Vector2(-speed, 0); bulletInstance.GetComponent<Rocket>().RocketPower = PlayerRocketPower;
                }
                if (isKeyDownUp == true && isKeyDownRight == true)//стрельба вверх и вправо
                {
                    nextFire = Time.time + fireRate; Rigidbody2D bulletInstance = Instantiate(rocket, new Vector2(transform.position.x + 1f, transform.position.y + 1f + Random.Range(-RandomFire, RandomFire)), Quaternion.Euler(new Vector3(0, 0, 26f))) as Rigidbody2D;
                    bulletInstance.velocity = new Vector2(speed * 1.5f, speed / 1.5f); bulletInstance.GetComponent<Rocket>().RocketPower = PlayerRocketPower;
                }
                if (isKeyDownUp == true && isKeyDownLeft == true)//стрельба вверх и влево
                {
                    nextFire = Time.time + fireRate; Rigidbody2D bulletInstance = Instantiate(rocket, new Vector2(transform.position.x - 1f, transform.position.y + 1f + Random.Range(-RandomFire, RandomFire)), Quaternion.Euler(new Vector3(0, 0, 150f))) as Rigidbody2D;
                    bulletInstance.velocity = new Vector2(-speed * 1.5f, speed / 1.5f); bulletInstance.GetComponent<Rocket>().RocketPower = PlayerRocketPower;
                }
                if (isKeyDownUp == true && isKeyDownRight == false && isKeyDownLeft == false)//стрельба вверх
                {
                    nextFire = Time.time + fireRate; Rigidbody2D bulletInstance = Instantiate(rocket, new Vector2(transform.position.x + Random.Range(-RandomFire, RandomFire), transform.position.y + 1.5f), Quaternion.Euler(new Vector3(0, 0, 90))) as Rigidbody2D;
                    bulletInstance.velocity = new Vector2(0, speed); bulletInstance.GetComponent<Rocket>().RocketPower = PlayerRocketPower;
                }
                if (isKeyDownUp == false && isKeyDownDow == true && isKeyDownRight == false && isKeyDownLeft == false && isGrounded == false)//стрельба вниз в прыжке
                {
                    nextFire = Time.time + fireRate; Rigidbody2D bulletInstance = Instantiate(rocket, new Vector2(transform.position.x + Random.Range(-RandomFire, RandomFire), transform.position.y - 1.5f), Quaternion.Euler(new Vector3(0, 0, -90))) as Rigidbody2D;
                    bulletInstance.velocity = new Vector2(0, -speed); bulletInstance.GetComponent<Rocket>().RocketPower = PlayerRocketPower;
                }
                if (isKeyDownUp == false && isKeyDownDow == true && isKeyDownRight == false && isKeyDownLeft == false && isGrounded == true && isPlayerLeftOrRight == true)//стрельба присев вправо на земле
                {
                    nextFire = Time.time + fireRate; Rigidbody2D bulletInstance = Instantiate(rocket, new Vector2(transform.position.x + 1f, transform.position.y - 0.5f + Random.Range(-RandomFire, RandomFire)), Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
                    bulletInstance.velocity = new Vector2(speed, 0); bulletInstance.GetComponent<Rocket>().RocketPower = PlayerRocketPower;
                }
                if (isKeyDownUp == false && isKeyDownDow == true && isKeyDownRight == false && isKeyDownLeft == false && isGrounded == true && isPlayerLeftOrRight == false)//стрельба присев влево на земле
                {
                    nextFire = Time.time + fireRate; Rigidbody2D bulletInstance = Instantiate(rocket, new Vector2(transform.position.x - 1f, transform.position.y - 0.5f + Random.Range(-RandomFire, RandomFire)), Quaternion.Euler(new Vector3(0, 0, 180f))) as Rigidbody2D;
                    bulletInstance.velocity = new Vector2(-speed, 0); bulletInstance.GetComponent<Rocket>().RocketPower = PlayerRocketPower;
                }
                if (isKeyDownUp == false && isKeyDownDow == true && isKeyDownRight == true)//стрельба вниз и вправо
                {
                    nextFire = Time.time + fireRate; Rigidbody2D bulletInstance = Instantiate(rocket, new Vector2(transform.position.x + 1f, transform.position.y - 1f + Random.Range(-RandomFire, RandomFire)), Quaternion.Euler(new Vector3(0, 0, -30f))) as Rigidbody2D;
                    bulletInstance.velocity = new Vector2(speed * 1.5f, -speed / 1.5f); bulletInstance.GetComponent<Rocket>().RocketPower = PlayerRocketPower;
                }
                if (isKeyDownUp == false && isKeyDownDow == true && isKeyDownLeft == true)//стрельба вниз и влево
                {
                    nextFire = Time.time + fireRate; Rigidbody2D bulletInstance = Instantiate(rocket, new Vector2(transform.position.x - 1f, transform.position.y - 1f + Random.Range(-RandomFire, RandomFire)), Quaternion.Euler(new Vector3(0, 0, -150f))) as Rigidbody2D;
                    bulletInstance.velocity = new Vector2(-speed * 1.5f, -speed / 1.5f); bulletInstance.GetComponent<Rocket>().RocketPower = PlayerRocketPower;
                }
            }
            ///////FIRE END
            ///////JumpDown
            if (isKeyDownUp == false && isKeyDownDow == true && onPlatform == true && isKeyDownJump == true)//Прыжок с платформы вниз
            {
                //this.GetComponent<BoxCollider2D>().enabled = false;
                playerJumped = false;
                //StartCoroutine("EnableCollising");
                jumpDown = true;
            }
            //Debug.Log(playerJumped);

        }
        else
        {
            if (Input.GetButtonDown(Horizontal))
            {
                CreatePlayer();

            }
            if (Input.GetKeyUp(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }



    IEnumerator EnableCollising()
    {
        yield return new WaitForSeconds(0.3f);
        //this.GetComponent<BoxCollider2D>().enabled = true;
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), jk.gameObject.GetComponent<BoxCollider2D>(), false);//исключаем колайдер

    }

    void LaunchProjectile()
    {
        Times = 0; //Debug.Log("FUCKKKKKK");
    }
    void OnCollisionStay2D(Collision2D other)
    {
        if (other.transform.tag == "Platform")
        {
            //Debug.Log(rb.velocity.y);
            //if (rb.velocity.y == 0)
            if (jumpDown == true)
            {
                Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), other.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер

                jk = other.gameObject;
                //Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), other.gameObject.GetComponent<BoxCollider2D>(), false);//исключаем колайдер
                jumpDown = false;
                StartCoroutine("EnableCollising");

            }

            if (rb.velocity.y > -0.01f && rb.velocity.y < 0.01f)
            {
                isGrounded = true;
                onPlatform = true;
            }
        }
        //if (other.transform.tag == "Floor")
        //{
        //    isGrounded = true;
        //}
        if (other.transform.tag == "MovedPlatform")
        {
            isGrounded = true; //Times = 5f; Invoke("LaunchProjectile", 0.5f);
        }
        if (other.transform.tag == "Wall")
        {
            isPlayerOnWall = true;
            isGrounded = true; Times = 5f;
            rb.velocity = Vector2.zero;
        }
    }


    void OnCollisionExit2D(Collision2D other)
    {
        if (other.transform.tag == "Floor")
        //{

        //    isGrounded = false;// Times = 0;
        //}
        if (other.transform.tag == "Platform")
        {
            //Debug.Log("11111111111111");
            if (jumpDown == true)
            {
                jk = other.gameObject;
                //Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), other.gameObject.GetComponent<BoxCollider2D>(), false);//исключаем колайдер
                jumpDown = false;
                StartCoroutine("EnableCollising");
                    Debug.Log("!!!!!!!!!!!!!!");

            }

            isGrounded = false;// Times = 0;
            onPlatform = false;
        }
        if (other.transform.tag == "Wall")
        {
            isPlayerOnWall = false;
            isGrounded = false;
            Invoke("LaunchProjectile", 0.5f);
        }
        if (other.transform.tag == "MovedPlatform" && Input.GetButton(Jump))
        {
            isGrounded = false; //Times = 5f; Invoke("LaunchProjectile", 0.5f);
        }

    }

    void OnTriggerStay2D(Collider2D other)
    {

        if (other.gameObject.tag == "Floor")
        {
            isGrounded = true;
        }


        if (other.gameObject.tag == "MovedPlatform")
        {
            transform.parent = other.transform;
            transform.rotation = Quaternion.Euler(0, 0, 0);
            //this.gameObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);


        }
        /////Squirrel
        if (sit == true)//other.gameObject.tag == "Enemy" && 
        {
            if (other.transform.GetComponent("JungleSquirrel") != null && other.gameObject.GetComponent<JungleSquirrel>().take == true)//если в врага есть скрипт белки и она мертва
            {
                //Debug.Log("BELKAAAAA");
                Belka++;
                other.gameObject.GetComponent<JungleSquirrel>().take = false;
                other.gameObject.GetComponent<JungleSquirrel>().Destroy();
            }

        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //if (other.gameObject.tag == "MovedPlatform" && Input.GetButton(Jump))
        if (other.gameObject.tag == "Floor")
        {
            isGrounded = true;
        }
    }


    void OnTriggerExit2D(Collider2D other)
    {
        //if (other.gameObject.tag == "MovedPlatform" && Input.GetButton(Jump))
        if (other.gameObject.tag == "MovedPlatform")
        {
            transform.parent = null;
            transform.rotation = Quaternion.Euler(0, 0, 0);
            //this.gameObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }
        if (other.gameObject.tag == "Floor")
        {
            isGrounded = false;
        }

    }

    void FixedUpdate()
    {


        if (Dead == false && cantMove == false)
        {
            //If our player is holding the sprint button, we've held down the button for a while, and we're grounded...
            //OR our player jumped while we were already sprinting...
            if (false && Input.GetButton(Vertical) && Time.time - sprintTimer > sprintDelay && isGrounded || jumpedDuringSprint)
            {
                //... then sprint
                // rb.velocity = new Vector2(Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime * sprintMultiplier, rb.velocity.y);
                Debug.Log(Horizontal);
                //If our player jumped during our sprint...
                if (playerJumped)
                {
                    jumpedDuringSprint = true; //... tell the game that we jumped during our sprint!
                                               //This is a tricky one. Basically, if we are already sprinting and our player jumps, we want them to hold their
                                               //momentum. Since they are no longer grounded, we would not longer return true on a regular sprint because
                                               //the build-up of sprint requires the player to be grounded. Likewise, if our player presses another horizontal
                                               //key, the jumpedDuringSprint would be set to false in our Update() function, thus causing a "loss" in momentum.
                }
            }
            else
            {
                //If we're not sprinting, then give us our general momentum
                //rb.velocity = new Vector2(Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime, rb.velocity.y);

                //if (PlayerCode == 0)
                //{
                //    Debug.Log("Speed - " + Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime);
                //}

                

                


                //if ((Input.GetAxis(Horizontal) < -0.1f) && (Speed > -MaxSpeed))
                if ((Input.GetAxis(Horizontal) < -0.1f))
                {
                    //Speed = Speed - (Acceleration * Time.deltaTime);
                    if (Speed > 0)
                    {
                        t = 0;
                    }
                    Speed = -MaxSpeed * (t * t * t * (t * (6f * t - 15f) + 10f));
                    if(t < 1f && t != 1f)
                    {
                        t = t + 0.1f;
                    }
                    
                }

                //else if ((Input.GetAxis(Horizontal) > 0.1f) && (Speed < MaxSpeed))
                else if ((Input.GetAxis(Horizontal) > 0.1f))
                {
                    if (Speed < 0)
                    {
                        t = 0;
                    }

                    //Speed = Speed + (Acceleration * Time.deltaTime);
                    Speed = MaxSpeed * (t * t * t * (t * (6f * t - 15f) + 10f));
                    if (t < 1f && t != 1f)
                    {
                        t = t + 0.1f;
                    }
                }

                //else if (Speed > (Deceleration * Time.deltaTime))
                else if (Speed > 0.01f)
                {
                    //Speed = Speed - (Deceleration * Time.deltaTime);
                    Speed = MaxSpeed * (t * t * t * (t * (6f * t - 15f) + 10f));
                    if (t > 0 && t != 0)
                    {
                        t = t - 0.2f;
                    }
                }

                //else if (Speed < (-Deceleration * Time.deltaTime))
                else if (Speed < -0.01f)
                {
                    //Speed = Speed + (Deceleration * Time.deltaTime);
                    Speed = -MaxSpeed * (t * t * t * (t * (6f * t - 15f) + 10f));
                    if (t > 0 && t != 0)
                    {
                        t = t - 0.2f;
                    }
                }

                else //if ((Input.GetAxis(Horizontal) < -0.1f) && (Input.GetAxis(Horizontal) > 0.1f))/// видалити!!!!!!!!!
                {
                    Speed = 0;
                    //t = 0;
                }



                //Debug.Log(Speed);

                //Vector2 PlayerScreenPosition = Camera.main.WorldToScreenPoint(this.transform.position);
                //Debug.Log("Speed - " + Speed + "  position - " + PlayerScreenPosition.x);

                //if (PlayerScreenPosition.x > 100 && Speed < 0 || PlayerScreenPosition.x < Screen.width - 100 && Speed > 0)
                //{
                //    rb.velocity = new Vector2(1 * Speed * Time.deltaTime, rb.velocity.y);
                //}

                //if (PlayerScreenPosition.x > 100 && Speed < 0 || PlayerScreenPosition.x < Screen.width - 100 && Speed > 0)
                //{
                //    rb.velocity = new Vector2(1 * Speed * Time.deltaTime, rb.velocity.y);
                //}



                rb.velocity = new Vector2(1 * Speed * Time.deltaTime, rb.velocity.y);


            }

            //If our player pressed the jump key...
            //if (playerJumped && Input.GetAxisRaw("Vertical") > -0.5f && onPlatform == false)
            //if (playerJumped && onPlatform == false) - old
            if (playerJumped && onPlatform == false && isPlayerOnWall == false)//isPlayerOnWall == false - что бы не прыгал вверх просто так
            {

                rb.velocity = Vector2.zero;
                rb.AddForce(new Vector2(0, initialJumpForce)); //"Jump" our player up in the air!
                playerJumped = false; //Our player already jumped, so no need to jump again just yet
            }
            if (playerJumped && onPlatform == true && Input.GetAxisRaw(Vertical) >= -0f)
            {
                rb.velocity = Vector2.zero;
                rb.AddForce(new Vector2(0, initialJumpForce)); //"Jump" our player up in the air!
                playerJumped = false; //Our player already jumped, so no need to jump again just yet
            }

            //If our player is holding the jump button and a little bit of time has passed...
            if (playerJumping && Time.time - jumpTimer > delayToExtraJumpForce && isPlayerOnWall == false)//isPlayerOnWall == false - что бы не прыгал вверх просто так
            {
                //Debug.Log("Hight jump");
                rb.AddForce(new Vector2(0, extraJumpForce)); //... then add some additional force to the jump
            }

            //Debug.Log(playerJumping);

            if (isKeyDownDow == true && isGrounded == false)
            {
                
                rb.AddForce(transform.up * 2f * -8, ForceMode2D.Impulse);//при прыжке если нажать вниз добавляем ускорение для быстрого опускания
            }


        }
    }
}
