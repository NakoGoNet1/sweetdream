﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorDamage : MonoBehaviour {

    public float HightEnemyJump = 5550f;
    // Use this for initialization
    void Start () {
		
	}

    void OnTriggerStay2D(Collider2D col)
    {

        if (col.tag == "Player")
        {

            col.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, true);
            //Debug.Log("111111111");
        }
    }

    void OnCollisionStay2D(Collision2D col)
    {
        if (col.transform.tag == "Player")
        {

            col.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, true);
            //Debug.Log("111111111");
        }
    }


    // Update is called once per frame
    void Update () {
		
	}
}
