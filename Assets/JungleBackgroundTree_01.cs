﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JungleBackgroundTree_01 : MonoBehaviour {


    Animator anim;
    public float Time;
    public float Sdvig;
    
    public GameObject Platform;
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {

        
        anim.Play("BackgroundTree_anim01", 0, (Platform.transform.position.y - Sdvig) * -1);
        Time = (Platform.transform.position.y - Sdvig) * -1;
    }
}
