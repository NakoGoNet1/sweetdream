﻿using UnityEngine;
using System.Collections;

public class MosquitoClass : MonoBehaviour
{
    public GameObject Player1; //подключаем игрока для понимания с какой он стороны для поворота врага
    public GameObject Player2;
    public GameObject PlayerCenter;
    public GameObject AggrePlayer;
    public int HP = 2;                  // How many times the enemy can be hit before it dies.
    public GameObject LeftBorder;
    public GameObject RightBorder;
    public GameObject TopBorder;
    public GameObject BottomBorder;
    //private int HP_enemy; //сколько жизней в врага в данный момент
    public float TimeChangePositionPublic = 2f;
    public float TimeChangePosition = 2f;
    public float BirthTime = 20f;
    private float transformx = 0;
    private float transformy = 0;
    public int pointx = 1;//точка где висит враг (0-лево, 1-право, 2 - на игрока летит, 3 - висит на месте потому как все игроки мертвы,
    //4 - игрок вышел за пределы поля врага и враг летит на свою 0.0 позицию, 5 - начало аттаки)
    private int point;
    private bool changePos = false;
    public float HightEnemyJump = 5550f;
    private bool visible = false;
    private bool death = true;
    private bool live = false;
    public float speedToBase = 0.05f;
    Animator anim;
    private string animState = "Idle";

    void OnDrawGizmosSelected()
    {


        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, new Vector3(LeftBorder.transform.position.x, LeftBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(RightBorder.transform.position.x, RightBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(TopBorder.transform.position.x, TopBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(transform.position, new Vector3(BottomBorder.transform.position.x, BottomBorder.transform.position.y, 0.0f));

        Gizmos.DrawLine(new Vector3(LeftBorder.transform.position.x, TopBorder.transform.position.y, 0.0f), new Vector3(RightBorder.transform.position.x, TopBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(new Vector3(RightBorder.transform.position.x, TopBorder.transform.position.y, 0.0f), new Vector3(RightBorder.transform.position.x, BottomBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(new Vector3(RightBorder.transform.position.x, BottomBorder.transform.position.y, 0.0f), new Vector3(LeftBorder.transform.position.x, BottomBorder.transform.position.y, 0.0f));
        Gizmos.DrawLine(new Vector3(LeftBorder.transform.position.x, BottomBorder.transform.position.y, 0.0f), new Vector3(LeftBorder.transform.position.x, TopBorder.transform.position.y, 0.0f));

    }
    void Start()
    {
        GetComponent<EnemyHealth>().HP_enemy = HP;
        point = pointx;
        anim = GetComponent<Animator>();
        TimeChangePosition = TimeChangePositionPublic;
    }

    private void SelectAggrePlayer()
    {
        //select aggrePlayer
        if (Player1.GetComponent<Mario1Controller2DScript>().Dead == false && Player2.GetComponent<Mario1Controller2DScript>().Dead == false)
        {
            //Debug.Log(Random.Range(-2f, 2f));
            if (Random.Range(-2f, 2f) > 0)
            {
                AggrePlayer = Player2;
            }
            else
            {
                AggrePlayer = Player1;
            }
        }
        else if (Player1.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            AggrePlayer = Player2;
        }
        else if (Player2.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            AggrePlayer = Player1;
        }
        if (Player1.GetComponent<Mario1Controller2DScript>().Dead == true && Player2.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            point = 3;
        }
    }

    IEnumerator waitToChangePos()
    {
        //if (death == false)
        //{
        yield return new WaitForSeconds(TimeChangePosition); changePos = false; //Debug.Log("!!!!!!!!!!!!!");
        switch (point)
        {
            case 0:
                point = 1;
                break;
            case 1:
                point = 5;
                break;
            case 2:
                point = 0;
                break;
            case 3:
                point = 3;
                break;
            case 4:
                point = 4;
                break;
            default:
                point = 0;
                break;
        }
        //}
        //Debug.Log("!!!!!!!!!!!!!!" + point);
    }

    private void Visible()
    {
        //Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (PlayerCenter.transform.position.x > LeftBorder.transform.position.x && PlayerCenter.transform.position.x < RightBorder.transform.position.x &&
            PlayerCenter.transform.position.y > BottomBorder.transform.position.y && PlayerCenter.transform.position.y < TopBorder.transform.position.y)
        {
            visible = true;
        }
        else
        {
            visible = false;
        }
    }
    
    private void CheckPlayerDead()
    {
        if (Player1.GetComponent<Mario1Controller2DScript>().Dead == true && Player2.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            point = 3;
        }
    }

    private void StartAttack()
    {
        point = 2;
    }

    private void Animation()
    {
        if (point == 5)
        {
            if (animState != "Attack_start")
            {
                anim.SetTrigger("Attack_start");
                animState = "Attack_start";
            }
        }
        else if (point == 0)
        {
            if (transform.position.x < AggrePlayer.transform.position.x)
            {
                if (animState != "Forward")
                {
                    anim.SetTrigger("Forward");
                    animState = "Forward";
                }
            }
            else
            {
                if (animState != "Back")
                {
                    anim.SetTrigger("Back");
                    animState = "Back";
                }
            }
        }
        else if (point == 1)
        {
            if (transform.position.x < AggrePlayer.transform.position.x)
            {
                if (animState != "Back")
                {
                    anim.SetTrigger("Back");
                    animState = "Back";
                }
            }
            else
            {
                if (animState != "Forward")
                {
                    anim.SetTrigger("Forward");
                    animState = "Forward";
                }
            }
        }

            

    }

    private void Update()
    {
        //Debug.Log(point);

        CheckPlayerDead();
        Animation();

        Visible();
        if (death == false && visible == false)
        {
            point = 4;
        }
        Visible();
        if (visible == true && point == 4)
        {
            death = true;
            live = true;
            //Debug.Log("1111111111");
            GetComponent<EnemyHealth>().HP_enemy = HP;
            death = false;
            point = 2;
            live = false;
            SelectAggrePlayer();//выбор игрока на которого нужно аггриться

        }
    }


    void FixedUpdate()
    {
        //проверка не умер ли игрок на какого мы агримся
        if (AggrePlayer == Player1 && Player1.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            if (point == 3)
            {
                point = 1;
            }
            AggrePlayer = Player2;
        }
        if (AggrePlayer == Player2 && Player2.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            if (point == 3)
            {
                point = 1;
            }
            AggrePlayer = Player1;
        }
        if (Player1.GetComponent<Mario1Controller2DScript>().Dead == true && Player2.GetComponent<Mario1Controller2DScript>().Dead == true)
        {
            point = 3;
        }


        ///Debug.Log(point + " death=" + death + " changePos=" + changePos);
        if (GetComponent<EnemyHealth>().HP_enemy <= 0 || death == true)//мы убили врага
        {
            //Debug.Log("!!!!!!!!!!!!!!");
            if (live == false)
            {
                //Debug.Log("!!!!!!!!!!!!!!");
            transform.localPosition = new Vector2(0, 0);
            //point = 3;
            Visible();
            if (visible == true)
            {
                    death = true;
                    live = true;
                    //Debug.Log("1111111111");
                    StartCoroutine("waitToBirth");
                
            }
            }

        }
        //Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        //Debug.Log("target is " + screenPosition.x + " pixels from the left" + Screen.width);

        if (transform.position.x < AggrePlayer.transform.position.x) // поворот врага в зависимости с какой стороны игрок
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
        }
        else
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }


        if (changePos == false)
        {
            if (point == 5)
            {
                TimeChangePosition = TimeChangePositionPublic * 2;
            }
            else if (point == 1)
            {
                TimeChangePosition = TimeChangePositionPublic / 1.7f;
            }
            else
            {
                TimeChangePosition = TimeChangePositionPublic;
            }
            StartCoroutine("waitToChangePos"); changePos = true;
        }
        

        //transformx = 150f - screenPosition.x;
        //transform.position = new Vector2(transform.position.x - (transformx / 25), transform.position.y);
        if (point == 1 && death == false)
        {
         transformx = (transform.position.x - AggrePlayer.transform.position.x) + 4f;
         transformy = (transform.position.y - AggrePlayer.transform.position.y) - 5f;
         //Debug.Log(transformx);
         transform.position = new Vector2(transform.position.x - (transformx / 50), transform.position.y - (transformy / 50));
        }
        if (point == 0 && death == false)
        {
            transformx = (transform.position.x - AggrePlayer.transform.position.x) - 10f;
            transformy = (transform.position.y - AggrePlayer.transform.position.y) - 5f;
            //Debug.Log(transformx);
            transform.position = new Vector2(transform.position.x - (transformx / 50), transform.position.y - (transformy / 50));
        }
        if (point == 2 && death == false)
        {
            transformx = transform.position.x - AggrePlayer.transform.position.x;
            transformy = transform.position.y - AggrePlayer.transform.position.y;
            
            transform.position = new Vector2(transform.position.x - (transformx / 50), transform.position.y - (transformy / 50));
        }
        if (point == 4 && death == false)
        {
            //transform.localPosition = new Vector2(transform.localPosition.x / 1.02f, transform.localPosition.y / 1.02f);

            transform.localPosition = Vector2.MoveTowards(transform.localPosition, new Vector2(0, 0), speedToBase);
        }



    }




    IEnumerator waitToBirth()
    {
        //Debug.Log("11111111111");
        yield return new WaitForSeconds(BirthTime); GetComponent<EnemyHealth>().HP_enemy = HP; death = false; point = pointx; live = false;
        SelectAggrePlayer();//выбор игрока на которого нужно аггриться
    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, false);
        }

    }
}