﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelJungle_BossCorgy : MonoBehaviour {




    public float moveSpeed = 1f;
    public GameObject Player1;
    public GameObject Player2;
    public GameObject PlayerCenter;
    public GameObject AggrePlayer;
    public GameObject Boss;
    public int HP = 2;
    public float BirthTime;
    public float HightEnemyJump = 5550f;
    Animator anim;
    Rigidbody2D rb;
    public bool side = true;
    public bool run = false;
    public bool live = false;

    private float dividerMove = 15f;

    // Use this for initialization
    void Start () {

        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player1.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), Player2.gameObject.GetComponent<BoxCollider2D>(), true);//исключаем колайдер врага и колайдер игрока
        rb.simulated = false;
    }
	
	// Update is called once per frame
	void Update () {

        CheckEnemyHP();

        if (live == true)
        {
            rb.simulated = true;
        }

    }

    private void CheckEnemyHP()
    {
        if (GetComponent<EnemyHealth>().HP_enemy <= 0)//если у врага нету жизней убиваем его
        {
            if (Boss.gameObject != null)
            {
                Boss.gameObject.GetComponent<LevelJungleBoss>().CorgyCount = Boss.gameObject.GetComponent<LevelJungleBoss>().CorgyCount - 1;
            }
            
            Destroy(gameObject);
        }
    }

    void FixedUpdate()
    {
        if (side == true)
        {
            if (run == true)
            {
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                rb.velocity = new Vector2(transform.localScale.x * moveSpeed / dividerMove, GetComponent<Rigidbody2D>().velocity.y);
                if (dividerMove <= 1f)
                {
                    dividerMove = 1f;
                }
                else
                {
                    dividerMove = dividerMove / 1.06f;
                }
            }


        }
        else
        {
            if (run == true)
            {
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                rb.velocity = new Vector2(transform.localScale.x * (-moveSpeed / dividerMove), GetComponent<Rigidbody2D>().velocity.y);
                if (dividerMove <= 1f)
                {
                    dividerMove = 1f;
                }
                else
                {
                    dividerMove = dividerMove / 1.06f;
                }
            }

        }
    }


    private void OnTriggerEnter2D(Collider2D collision)//обработка столкновений
    {
        if (collision.gameObject.tag == "Player")//если мы наткнулись на игрока
        {
            collision.gameObject.GetComponent<Mario1Controller2DScript>().Damage(HightEnemyJump, false);//приносим ему урон
        }
        if (collision.gameObject.tag == "EnemyReturnColl")//если мы наткнулись на коллайдер розворота врага
        {
            dividerMove = 15f;
            GetComponent<Rigidbody2D>().AddForce(transform.up * 2f * 4, ForceMode2D.Impulse);//добавляем вектор подпрыга
            if (side == true)//крутим врага в сторону когда он ударился в стенку
            {
                side = false;
            }
            else
            {
                side = true;
            }
        }
        if (collision.gameObject.tag == "EnemyJumpColl")//если мы наткнулись на коллайдер подпрыга врага
        {
            GetComponent<Rigidbody2D>().AddForce(transform.up * 2f * 2, ForceMode2D.Impulse);//добавляем вектор подпрыга
           // anim.SetTrigger("Jump");
        }

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Floor" || other.gameObject.tag == "MovedPlatform")//
        {
            anim.SetTrigger("run");
            run = true;
        }

    }
}
