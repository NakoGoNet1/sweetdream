﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelJungleBoss_floor : MonoBehaviour
{



    public GameObject Boss;
    public int setPhaseToBoss;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (Boss.gameObject != null)
        {
            if (other.gameObject.tag == "Player" && Boss.gameObject.GetComponent<LevelJungleBoss>().phase == 100)
            {
                if (setPhaseToBoss == 5)
                {
                    Boss.gameObject.GetComponent<EnemyHealth>().HP_enemy = Boss.gameObject.GetComponent<LevelJungleBoss>().HP_phase3;
                    Boss.gameObject.GetComponent<LevelJungleBoss>().startAnim = false;
                    Boss.gameObject.GetComponent<LevelJungleBoss>().phase = setPhaseToBoss;
                }

                if (setPhaseToBoss == 0)
                {
                    //Boss.gameObject.GetComponent<EnemyHealth>().HP_enemy = Boss.gameObject.GetComponent<LevelJungleBoss>().HP_phase3;
                    //Boss.gameObject.GetComponent<LevelJungleBoss>().startAnim = false;
                    Boss.gameObject.GetComponent<LevelJungleBoss>().phase = setPhaseToBoss;
                }


            }
        }


    }


}
