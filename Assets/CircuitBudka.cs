﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitBudka : MonoBehaviour {


    public GameObject anchor;

    Animator anim;
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        //anim.Play("Temp_rotate_move2", 0, Random.Range(0, 40));
        anim.Play("Temp_rotate_move", 0, Random.Range(0.0f, 0.2f));
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        transform.position = new Vector3(anchor.transform.position.x, anchor.transform.position.y, transform.position.z);
        //anim.Play("Temp_rotate_move", 0, Random.Range(0.0f, 1.0f));
    }
}
