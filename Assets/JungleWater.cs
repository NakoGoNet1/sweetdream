﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JungleWater : MonoBehaviour {

    public GameObject Fish;
    // Use this for initialization
    void Start () {
		
	}


    void OnTriggerStay2D(Collider2D col)
    {

        if (col.tag == "Player")
        {

            Fish.GetComponent<Fish>().Damage = true;
            Fish.GetComponent<Fish>().AggrePlayer = col.gameObject;
            //Debug.Log("111111111");
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {

        if (col.tag == "Player")
        {

            Fish.GetComponent<Fish>().Damage = false;
            //Debug.Log("111111111");
        }
    }




    // Update is called once per frame
    void Update () {
		
	}
}
